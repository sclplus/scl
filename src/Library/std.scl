// ====================================================
// SCL+ standard library
// ====================================================

// Imports
import System
::Convert
::Console
::Math
::Random;

import System.Windows
::MessageBox;

/**
 * Print a line on the console
 * @param {value} Value to print, a new line will be added
 */
function println(value) {
	Console.WriteLine(value);
}

/**
 * Print on the console
 * @param {value} Value to print
 */
function print(value) {
	Console.WriteLine(value);
}

/**
 * Show a messagebox on the screen, modal
 * @param {text} Message box text
 * @param {title} Message title
 */
function showMsgBox(text, title) {
	MessageBox.Show(text, title);
}

/**
 * Get random value between a min and max value with a given step size
 * @param {min} minimal random value
 * @param {max} maximum random value
 * @param {step} step size
 * @return {double} random value 
 */
function random(min, max, step) {
	var rnd = new Random();
	var range = max - min;
	var steps = 1 + int(Math.Round(range / step));
	return min + step * rnd.Next(steps);
}

/**
 * Convert to string
 * @param {value} Value to convert to a string
 * @return {string} Converted string.
 */
function str(value) {
	return Convert.ToString(value);
}

/**
 * Convert to integer
 * @param {value} Value to convert to a integer
 * @return {string} Converted integer.
 */
function int(value) {
	return Convert.ToInt32(value);
}

/**
 * Convert to long
 * @param {value} Value to convert to a long
 * @return {string} Converted long.
 */
function long(value) {
	return Convert.ToInt64(value);
}

/**
 * Convert to float
 * @param {value} Value to convert to a float
 * @return {string} Converted long.
 */
function float(value) {
	return Convert.ToFloat(value);
}

/**
 * Convert to double
 * @param {value} Value to convert to a double
 * @return {string} Converted double.
 */
function double(value) {
	return Convert.ToDouble(value);
}

/**
 * Convert to bool
 * @param {value} Value to convert to a bool
 * @return {bool} Converted bool.
 */
function bool(value) {
	return Convert.ToBoolean(value);
}