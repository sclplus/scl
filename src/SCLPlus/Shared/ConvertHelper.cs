﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Shared
{
    public class ConvertHelper
    {
        public static float ParseFloat(string val)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            return float.Parse(val, NumberStyles.Any, ci);
        }

        public static double ParseDouble(string val)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            return double.Parse(val, NumberStyles.Any, ci);
        }

        public static bool TryParseFloat(string val, out float floatVal)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            return float.TryParse(val, NumberStyles.Any, ci, out floatVal);
        }

        public static bool TryParseDouble(string val, out double doubleVal)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            return double.TryParse(val, NumberStyles.Any, ci, out doubleVal);
        }
    }
}
