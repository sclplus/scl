﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Enum containing all token kinds/types</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Contains all token types
    /// </summary>
    public enum TokenType
    {
        Dot = 1,
        Semicolon = 2,
        Comma = 3,
        NewLine = 4,
        Colon = 5,
        DoubleColon = 6,

        OpenBracket = 20,
        CloseBracket = 21,
        OpenParenthesis = 22,
        CloseParenthesis = 23,
        OpenBraceToken = 24,
        CloseBraceToken = 25,

        Add = 30,
        Subtract = 31,
        Multiply = 32,
        Devide = 33,
        Assign = 34,

        Equal = 40,
        Unequal = 41,
        Greater = 42,
        Smaller = 43,
        GreaterEqual = 44,
        SmallerEqual = 45,

        And = 46,
        Or = 47,

        Set = 48,

        Constant = 400,
        Name = 500,

        KeywordImport = 999,
        
        KeywordDeclVar = 1000,
        KeywordDeclFunc = 1001,
        KeywordReturn = 1002,

        KeywordNew = 2000
    }

    /// <summary>
    ///  Defines the types of brackets
    /// </summary>
    public enum BracketType
    {
        Open = 0,
        Close = 1
    }

    public enum BracketProcessType
    { 
        CreateArray = 0,
        AccessIndex = 1
    }
}
