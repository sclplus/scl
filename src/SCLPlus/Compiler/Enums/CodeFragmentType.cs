﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Containing all code fragment types</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Code fragment types
    /// </summary>
    public enum CodeFragmentType
    {
        /// <summary>
        /// General
        /// </summary>
        None = 0,

        /// <summary>
        /// Defines, that the code fragment only can contains a boolean expression
        /// </summary>
        BooleanExpress = 5,

        /// <summary>
        /// Defines, that the code fragment only can contains executable code
        /// </summary>
        Command = 7
    }
}
