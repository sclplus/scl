﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>List of all syntax node types</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// List of every syntax node type
    /// </summary>
    public enum SyntaxNodeType
    {
        EntryPoint = 0,
        Operator = 1,
        Parenthesis = 2,

        Constant = 3,

        Set = 4,
        Accessor = 5,
        NamespaceAccessor = 6,

        FuncCall = 10,
        FuncDecl = 11,
        Argument = 12,
        Return = 13,

        Variable = 20,
        VariableDecl = 21,

        AnonymousObject = 40,
        AnonymProperty = 41,

        PropertyAccessor = 50,
        CallMethod = 51,
        
        Import = 60,
        ClrNamespace = 61,
        ClrClass = 62,
        
        CallClass = 70,
        New = 71,
        CreateInstance = 72,

        CreateArray = 80,
        ArrayItem = 81,
        IndexAccess = 82
    }
}
