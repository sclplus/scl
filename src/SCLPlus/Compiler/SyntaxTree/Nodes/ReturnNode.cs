﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class ReturnNode : SyntaxTreeNode
    {
        public ReturnNode(SyntaxTreeNode parent, RawToken token)
            : base(parent, SyntaxNodeType.Return, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "Return";
            }
        }
    }
}
