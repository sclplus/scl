﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class CreateArrayNode : SyntaxTreeNode
    {
        public CreateArrayNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.CreateArray, token)
        {

        }
        
        public override string DebugText
        {
            get { return "CreateArray"; }
        }

        public override void CheckSemantic()
        {

        }
    }
}
