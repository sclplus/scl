﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class NewNode : SyntaxTreeNode
    {
        public NewNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.New, token)
        {

        }

        public override string DebugText
        {
            get { return "new"; }
        }

        public override void CheckSemantic()
        {

        }
    }
}
