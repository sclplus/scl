﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class CallClassNode : SyntaxTreeNode
    {
        public CallClassNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.CallClass, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "CallClassNode (" + Token.Content + ")";
            }
        }

        public SyntaxTreeNode Class
        {
            get;
            set;
        }
    }
}
