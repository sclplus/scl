﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class ImportNode : SyntaxTreeNode
    {
        public ImportNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.Import, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        public override string DebugText
        {
            get { return "Import"; }
        }
    }
}
