﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class IndexAccessNode : SyntaxTreeNode
    {
        public IndexAccessNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.IndexAccess, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        public override string DebugText
        {
            get { return "IndexAccess"; }
        }
    }
}
