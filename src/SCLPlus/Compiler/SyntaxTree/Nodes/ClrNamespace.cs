﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class ClrNamespace : SyntaxTreeNode
    {
        public ClrNamespace(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.ClrNamespace, token)
        {

        }

        public string GetPath()
        {
            string rt = "";

            if (ParentNode is AccessorNode)
            {
                rt += (ParentNode as AccessorNode).GetPath();
            }

            rt += Token.Content;

            return rt;
        }

        public override void CheckSemantic()
        {

        }

        public override string DebugText
        {
            get { return "ClrNS::" + Token.Content; }
        }
    }
}
