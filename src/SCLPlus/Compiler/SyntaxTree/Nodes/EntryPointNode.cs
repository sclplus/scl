﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class EntryPointNode : SyntaxTreeNode, IScopeNode
    {
        public EntryPointNode()
            : base(null, SyntaxNodeType.EntryPoint, null)
        {
            SymbolTable = new SymbolTable(null);
        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "EntryPoint";
            }
        }

        public SymbolTable SymbolTable
        {
            get;
            private set;
        }
    }
}