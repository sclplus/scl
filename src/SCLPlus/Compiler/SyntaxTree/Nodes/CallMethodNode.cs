﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class CallMethodNode : SyntaxTreeNode
    {

        public CallMethodNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.CallMethod, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "CallMethod (" + this.Token.Content + ")";
            }
        }
    }
}
