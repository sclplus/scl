﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class PropertyAccessorNode : SyntaxTreeNode
    {
        public PropertyAccessorNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.PropertyAccessor, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "PropertyAcsr (" + this.Token.Content + ")";
            }
        }

       /* public VariableAccessType AccessType
        {
            get;
            set;
        }*/
    }
}
