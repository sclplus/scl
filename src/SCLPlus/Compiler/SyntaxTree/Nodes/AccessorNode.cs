﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class AccessorNode : SyntaxTreeNode
    {
        public AccessorNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.Accessor, token)
        {

        }

        public string GetPath()
        {
            string rt = "";

            if (ParentNode is ClrNamespace)
            {
                rt += (ParentNode as ClrNamespace).GetPath();
            }

            rt += ".";

            return rt;
        }

        public override void CheckSemantic()
        {

        }

        public override string DebugText
        {
            get { return "Accessor"; }
        }
    }
}
