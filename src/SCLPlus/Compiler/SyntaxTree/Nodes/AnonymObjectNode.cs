﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class AnonymObjectNode : SyntaxTreeNode
    {
        public AnonymObjectNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.AnonymousObject, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "AnonymObject";
            }
        }
    }
}
