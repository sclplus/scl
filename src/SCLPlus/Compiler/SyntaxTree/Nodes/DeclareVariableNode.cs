﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class DeclareVariableNode : SyntaxTreeNode
    {
        public DeclareVariableNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.VariableDecl, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "DeclareVariable (" + this.Token.Content + ")";
            }
        }
    }
}
