﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class DeclareFunctionNode : SyntaxTreeNode, IScopeNode
    {
        public DeclareFunctionNode(SyntaxTreeNode parent, RawToken token, SymbolTable symbolTable)
            : base(parent, SyntaxNodeType.FuncDecl, token)
        {
            SymbolTable = new SymbolTable(symbolTable);
        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "DeclareFunction (" + this.Token.Content + ")";
            }
        }

        public SymbolTable SymbolTable
        {
            get;
            private set;
        }
    }
}