﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class CreateInstanceNode : SyntaxTreeNode
    {

        public CreateInstanceNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.CreateInstance, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        public SyntaxTreeNode Class
        {
            get;
            set;
        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "CIN (" + this.Token.Content + ")";
            }
        }
    }
}
