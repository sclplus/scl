﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class ArgumentNode : SyntaxTreeNode
    {
        public ArgumentNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.Argument, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "Argument (" + (this.Token == null ? "<<Unnamed>>" : this.Token.Content) + ")";
            }
        }
    }
}
