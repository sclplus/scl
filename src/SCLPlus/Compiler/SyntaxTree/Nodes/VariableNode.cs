﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /*public enum VariableAccessType
    {
        Get,
        Set
    }*/

    public class VariableNode : SyntaxTreeNode
    {
        public VariableNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.Variable, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "Var (" + this.Token.Content + ")";
            }
        }

        /*public VariableAccessType AccessType
        {
            get;
            set;
        }*/
    }
}
