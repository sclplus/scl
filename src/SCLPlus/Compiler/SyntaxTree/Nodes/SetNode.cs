﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class SetNode : SyntaxTreeNode
    {
        public SetNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.Set, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        public override string DebugText
        {
            get { return "Set"; }
        }
    }
}
