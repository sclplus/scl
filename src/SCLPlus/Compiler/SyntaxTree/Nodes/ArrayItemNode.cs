﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class ArrayItemNode : SyntaxTreeNode
    {
        public ArrayItemNode(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.ArrayItem, token)
        {

        }


        public override void CheckSemantic()
        {

        }

        public override string DebugText
        {
            get { return "ArrayItem"; }
        }
    }
}
