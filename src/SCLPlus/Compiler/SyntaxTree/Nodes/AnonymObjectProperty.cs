﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class AnonymObjectProperty : SyntaxTreeNode
    {
        public AnonymObjectProperty(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.AnonymProperty, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        /// <summary>
        /// Get debug text
        /// </summary>
        public override string DebugText
        {
            get
            {
                return "AnonymProperty (" + this.Token.Content + ")";
            }
        }
    }
}
