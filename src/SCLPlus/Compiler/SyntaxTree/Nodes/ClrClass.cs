﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    public class ClrClass : SyntaxTreeNode
    {
        public ClrClass(SyntaxTreeNode Parent, RawToken token)
            : base(Parent, SyntaxNodeType.ClrClass, token)
        {

        }

        public override void CheckSemantic()
        {

        }

        public string GetPath()
        {
            string rt = "";

            if (ParentNode is NamespaceAccessorNode)
            {
                rt += (ParentNode as NamespaceAccessorNode).GetPath();
            }

            rt += Token.Content;

            return rt;
        }

        public string Name
        {
            get;
            set;
        }

        public override string DebugText
        {
            get { return "ClrClass::" + Token.Content; }
        }
    }
}
