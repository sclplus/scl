﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>SyntaxTreeBuilder create a sntax tree out of a list of raw tokens</summary>

using SCLPlus.Collections.Generic;
using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// SyntaxTreeBuilder create a sntax tree out of a list of raw tokens
    /// </summary>
    internal class SyntaxTreeBuilder
    {
        #region Private Member
        private Dequeue<RawToken> tokens;
        private ParserConfiguration parserConfig;
        private EntryPointNode entryPoint;
        private Stack<IScopeNode> scopeNodes;
        private IErrorListener errorListener;
        #endregion

        #region Constructor
        /// <summary>
        /// Create syntax tree builder
        /// </summary>
        public SyntaxTreeBuilder(ParserConfiguration parserConfig, Dequeue<RawToken> tokens, IErrorListener errorListener)
        {
            this.tokens = tokens;
            this.parserConfig = parserConfig;
            this.scopeNodes = new Stack<IScopeNode>();
            this.errorListener = errorListener;
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        /// <summary>
        /// Create the syntax tree
        /// </summary>
        /// <param name="codeFragment">Fragment to build. This is important for the entry point of the script part</param>
        public EntryPointNode Build()
        {
            #region [Post-process tokens]
            Dequeue<RawToken> processedTokens = new Dequeue<RawToken>();

            // Token post-process
            while (tokens.Count > 0)
            {
                RawToken token = tokens.PopFirst();

                #region [Create decimal number (1 . 2 ==> 1.2)]
                if (token.Content == ".")
                {
                    if (processedTokens.Count > 0 && parserConfig.IsInt64(processedTokens.PeekLast().Content))
                    {
                        if (tokens.Count > 0)
                        {
                            RawToken lastProcssedToken = processedTokens.PopLast();
                            RawToken nextToken = tokens.PopFirst();

                            if (parserConfig.IsInt64(nextToken.Content) == false && nextToken.Content.EndsWith("d") == false && nextToken.Content.EndsWith("f"))
                            {
                                errorListener.Report("T0001", "Syntax error, exptected numeric value: " + nextToken.Content, nextToken.Index.Item1, nextToken.Index.Item2, nextToken);
                            }

                            token = new RawToken(lastProcssedToken.Content + token.Content + nextToken.Content, null, new Tuple<int, int>(lastProcssedToken.Index.Item1, nextToken.Index.Item2));
                        }
                        else
                        {
                            errorListener.Report("T0005", "Syntax error, expected token behind: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                        }
                    }
                }
                #endregion

                #region [Classify tokens]
                string tcontent = token.Content;


                var constType = parserConfig.GetConstDataType(token);

                if (constType != DataType.None)
                {
                    token.Type = TokenType.Constant;
                }
                else if (SyntaxTreeFactory.Singleton.StaticTokens.ContainsKey(tcontent))
                {
                    token.Type = SyntaxTreeFactory.Singleton.StaticTokens[tcontent].Type;
                }
                else if (parserConfig.IsValidLanguageIndependentIdentifier(tcontent))
                {
                    token.Type = TokenType.Name;
                }
                else
                {
                    errorListener.Report("T0006", "Invalid token: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                }
                #endregion

                processedTokens.PushBack(token);
            }

            tokens = processedTokens;
            #endregion

            // Create entry-point
            this.entryPoint = new EntryPointNode();
            this.scopeNodes.Push(this.entryPoint);

            // Parse all tokens
            while (tokens.Count > 0)
            {
                // Create syntaxtreenode from tokens
                Parse(this.entryPoint, tokens, entryPoint.SymbolTable);
            }

            return entryPoint;
        }

        public SyntaxTreeNode Parse(SyntaxTreeNode parent, Dequeue<RawToken> tokens, SymbolTable symbolTable)
        {
            IList<SyntaxTreeNode> treeNodeList = new List<SyntaxTreeNode>();
            SyntaxTreeNode returnNode = parent;

            while (tokens.Count > 0)
            {
                RawToken token = tokens.PopFirst();
                bool exitLoop = false;

                switch (token.Type)
                {
                    #region [Ignore tokens]
                    case TokenType.NewLine:
                        break;
                    #endregion

                    #region [TokenType.KeywordImport]
                    case TokenType.KeywordImport:
                        {
                            if (tokens.Count == 0)
                            {
                                errorListener.Report("ST0021", "Expected import path" + token.Content, token.Index.Item1, token.Index.Item2, token);
                            }

                            returnNode = parent.CreateChildNode<ImportNode>(token);
                        }
                        break;
                    #endregion

                    #region [TokenType.KeywordDeclFunc]
                    case TokenType.KeywordDeclFunc:
                        {
                            if (tokens.Count == 0)
                            {
                                errorListener.Report("ST0001", "No function name found after function keyword: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                            }

                            // Get function name
                            RawToken funcName = tokens.PopFirst();
                            DeclareFunctionNode declFuncNode = parent.CreateChildNode<DeclareFunctionNode>(funcName, entryPoint.SymbolTable);

                            // Set current scope
                            this.scopeNodes.Push(declFuncNode);

                            // Get arguments
                            Dequeue<RawToken> argTokens = GetInnerTokens(tokens, TokenType.OpenParenthesis, TokenType.CloseParenthesis);

                            // Remove Parenthesis-Token
                            argTokens.PopFirst();
                            argTokens.PopLast();

                            while (argTokens.Count > 0)
                            {
                                RawToken argToken = argTokens.PopFirst();

                                if (argToken.Type == TokenType.Name)
                                {
                                    ArgumentNode argNode = declFuncNode.CreateChildNode<ArgumentNode>(argToken);
                                    entryPoint.SymbolTable.AddArgumentSymbol(argNode);
                                }
                                else if (argToken.Type != TokenType.Comma && argToken.Type != TokenType.NewLine)
                                {
                                    errorListener.Report("ST0002", "Unexpected token in argument list: " + argToken.Content, argToken.Index.Item1, argToken.Index.Item2, argToken);
                                }
                            }

                            // Get Function inner block
                            Dequeue<RawToken> contentTokens = GetInnerTokens(tokens, TokenType.OpenBraceToken, TokenType.CloseBraceToken);

                            // Remove Brace-Token
                            contentTokens.PopFirst();
                            contentTokens.PopLast();

                            // Parse inner 
                            while (contentTokens.Count > 0)
                            {
                                Dequeue<RawToken> line = GetUntilTokens(contentTokens, TokenType.Semicolon);

                                if (line.PeekLast().Type == TokenType.Semicolon)
                                {
                                    // Remove ;
                                    line.PopLast();
                                }

                                Parse(declFuncNode, line, declFuncNode.SymbolTable);
                            }

                            // Remove current scope
                            this.scopeNodes.Pop();

                            returnNode = declFuncNode;
                        }
                        break;
                    #endregion

                    #region [TokenType.KeywordReturn]
                    case TokenType.KeywordReturn:
                        {
                            ReturnNode rtNode = parent.CreateChildNode<ReturnNode>(token);

                            Parse(rtNode, tokens, symbolTable);

                            returnNode = rtNode;
                        }
                        break;
                    #endregion

                    #region [TokenType.KeywordDeclVar]
                    // if token is decalre varable key-word
                    case TokenType.KeywordDeclVar:
                        {
                            if (tokens.Count == 0)
                            {
                                errorListener.Report("ST0003", "Expected variable name: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                            }

                            RawToken varName = tokens.PopFirst();

                            if (!parserConfig.IsValidLanguageIndependentIdentifier(varName.Content))
                            {
                                errorListener.Report("ST0004", "Invalid variable name: " + varName.Content, varName.Index.Item1, varName.Index.Item2, varName);
                            }

                            if (tokens.Count == 0)
                            {
                                errorListener.Report("ST0005", "Expected '=' token: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                            }

                            DeclareVariableNode declVarNode = parent.CreateChildNode<DeclareVariableNode>(varName);

                            // Add to current scope
                            scopeNodes.Peek().SymbolTable.AddVariableSymbol(declVarNode);

                            Parse(declVarNode, tokens, symbolTable);

                            returnNode = declVarNode;

                            // Exit parsing
                            exitLoop = true;
                        }
                        break;
                    #endregion

                    #region [TokenType.OpenBrace / CloseBrace]
                    case TokenType.OpenBraceToken:
                        {
                            // Get token inner
                            tokens.PushFront(token);
                            Dequeue<RawToken> braceInnerTokens = GetInnerTokens(tokens, TokenType.OpenBraceToken, TokenType.CloseBraceToken);

                            // Remove open close brace
                            braceInnerTokens.PopFirst();
                            braceInnerTokens.PopLast();

                            // If the parent node is a var declaration, we will get an anonym object
                            if (parent is SetNode)
                            {
                                AnonymObjectNode aoNode = parent.CreateChildNode<AnonymObjectNode>(token);

                                while (braceInnerTokens.Count > 0)
                                {
                                    // Parse inner
                                    Parse(aoNode, braceInnerTokens, symbolTable);
                                }
                            }
                        }
                        break;
                    #endregion

                    #region [TokenType.Dot]
                    case TokenType.DoubleColon:
                        {
                            if ((returnNode is NamespaceAccessorNode) == false)
                            {
                                returnNode = returnNode.CreateChildNode<NamespaceAccessorNode>(token);
                            }
                        }
                        break;

                    case TokenType.Dot:
                        {
                            returnNode = returnNode.CreateChildNode<AccessorNode>(token);
                        }
                        break;
                    #endregion

                    #region [TokenType.Set]
                    case TokenType.Set:
                        {
                            returnNode = returnNode.CreateChildNode<SetNode>(token);

                            Parse(returnNode, tokens, symbolTable);

                            exitLoop = true;
                        }
                        break;
                    #endregion

                    #region [TokenType.KeywordNew]
                    case TokenType.KeywordNew:
                        {
                            returnNode = new NewNode(null, token);

                            Parse(returnNode, tokens, symbolTable);

                            treeNodeList.Add(returnNode);

                            exitLoop = true;
                        }
                        break;
                    #endregion

                    #region [TokenType.Name]
                    case TokenType.Name:
                        {
                            if (tokens.Count > 0 && tokens.PeekFirst().Type == TokenType.OpenParenthesis)
                            {
                                SyntaxTreeNode callNode;

                                if (parent is NewNode)
                                {
                                    callNode = parent.CreateChildNode<CreateInstanceNode>(token);
                                    var classToken = symbolTable.FindSymbol(token.Content);

                                    if (classToken is ClassSymbol)
                                    {
                                        (callNode as CreateInstanceNode).Class = (classToken as ClassSymbol).TreeNode;
                                    }
                                    else
                                    {
                                        errorListener.Report("ST0020", "Class symbol not found: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                                    }
                                }
                                else if (returnNode is AccessorNode)
                                {
                                    callNode = returnNode.CreateChildNode<CallMethodNode>(token);
                                }
                                else
                                {
                                    callNode = new CallFunctionNode(null, token);
                                }

                                // Get Parameter-List Add token temp
                                Dequeue<RawToken> argTokens = GetInnerTokens(tokens, TokenType.OpenParenthesis, TokenType.CloseParenthesis);

                                // Remove ()
                                argTokens.PopFirst();
                                argTokens.PopLast();

                                // Get arguments
                                IList<Dequeue<RawToken>> argList = GetArguments(argTokens);

                                foreach (Dequeue<RawToken> atk in argList)
                                {
                                    ArgumentNode node = callNode.CreateChildNode<ArgumentNode>(null);

                                    Parse(node, atk, symbolTable);
                                }

                                if ((returnNode is AccessorNode) == false && (callNode is CreateInstanceNode)  == false)//!ShouldBeProperty(returnNode))
                                {
                                    treeNodeList.Add(callNode);
                                }

                                returnNode = callNode;
                            }

                            // Define dynamic properties in dynamic classes
                            else if (tokens.Count > 0 && tokens.PeekFirst().Type == TokenType.Colon)
                            {
                                if (parent is AnonymObjectNode == false)
                                {
                                    errorListener.Report("ST0011", "Anonym properties can only be defined in anonym objects", token.Index.Item1, token.Index.Item2, token);
                                }

                                AnonymObjectProperty setAnonymProperty = parent.CreateChildNode<AnonymObjectProperty>(token);

                                SetNode setNode = setAnonymProperty.CreateChildNode<SetNode>(tokens.PopFirst());

                                Parse(setNode, tokens, symbolTable);

                                returnNode = setNode;
                            }

                            else
                            {
                                if (returnNode is ImportNode || (returnNode is AccessorNode && returnNode.ParentNode is ClrNamespace))
                                {
                                    returnNode = returnNode.CreateChildNode<ClrNamespace>(token);
                                }
                                else if (returnNode is AccessorNode)
                                {
                                    var setPropNode = returnNode.CreateChildNode<PropertyAccessorNode>(token);
                                    returnNode = setPropNode;
                                }
                                else if (returnNode is NamespaceAccessorNode)
                                {
                                    var clrClassNode = returnNode.CreateChildNode<ClrClass>(token);

                                    // Add class
                                    entryPoint.SymbolTable.AddClassSymbol(clrClassNode);
                                }
                                else
                                {
                                    var symbol = symbolTable.FindSymbol(token.Content);

                                    if (symbol == null || symbol is VariableSymbol)
                                    {
                                        if (symbol == null)
                                        {
                                            errorListener.Report("ST0015", "Symbol not found: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                                        }

                                        var setVar = new VariableNode(null, token);
                                        returnNode = setVar;

                                        treeNodeList.Add(setVar);
                                        if (tokens.Count > 0 && tokens.PeekFirst().Type == TokenType.Semicolon)
                                        {
                                            break;
                                        }
                                    }
                                    else if (symbol is ClassSymbol)
                                    {
                                        var classNode = parent.CreateChildNode<CallClassNode>(token);
                                        classNode.Class = (symbol as ClassSymbol).TreeNode;

                                        returnNode = classNode;
                                    }
                                }
                            }
                        }
                        break;
                    #endregion

                    #region [TokenType.Constant]
                    case TokenType.Constant:
                        {
                            var constNode = new ConstantNode(null, token);

                            var constType = constNode.DataType = parserConfig.GetConstDataType(token);

                            if (constType == DataType.None)
                            {
                                constNode.DataType = DataType.None;
                                errorListener.Report("ST0010", "Could not detect format for constant: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                            }

                            treeNodeList.Add(constNode);
                        }
                        break;
                    #endregion

                    #region [TokenType.Operator]
                    case TokenType.Add:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Add;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Subtract:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Sub;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Multiply:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Mul;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Devide:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Div;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;
                    case TokenType.Equal:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Equal;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Unequal:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Unequal;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Greater:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Greater;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Smaller:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Smaller;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.GreaterEqual:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.GreaterEqual;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.SmallerEqual:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.SmallerEqual;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;
                    case TokenType.And:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.And;
                            opNode.Association = OperatorAssociation.Left;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.Or:
                        {
                            OperatorNode opNode = new OperatorNode(null, token);
                            opNode.OperatorType = OperatorType.Or;
                            opNode.Association = OperatorAssociation.Left;
                            treeNodeList.Add(opNode);
                        }
                        break;
                    #endregion

                    #region [TokenType.Parenthesis]
                    case TokenType.OpenParenthesis:
                        {
                            ParenthesisNode opNode = new ParenthesisNode(null, token);
                            opNode.Type = BracketType.Open;

                            treeNodeList.Add(opNode);
                        }
                        break;

                    case TokenType.CloseParenthesis:
                        {
                            ParenthesisNode opNode = new ParenthesisNode(null, token);
                            opNode.Type = BracketType.Close;

                            treeNodeList.Add(opNode);
                        }
                        break;
                    #endregion

                    #region [TokenType.Parenthesis]
                    case TokenType.OpenBracket:
                        {
                            SyntaxTreeNode indexParentNode = null;
                            BracketProcessType processType;

                            if (returnNode is CallFunctionNode || returnNode is CallMethodNode || returnNode is VariableNode || returnNode is PropertyAccessorNode || returnNode is CreateArrayNode || returnNode is IndexAccessNode)
                            {
                                processType = BracketProcessType.AccessIndex;
                                indexParentNode = returnNode.CreateChildNode<IndexAccessNode>(token);
                            }
                            else
                            {
                                processType = BracketProcessType.CreateArray;
                                indexParentNode = parent.CreateChildNode<CreateArrayNode>(token);
                            }

                            // Add [ again
                            tokens.PushFront(token);

                            // Get inner tokens
                            var bracketTokens = GetInnerTokens(tokens, TokenType.OpenBracket, TokenType.CloseBracket);

                            // Remove front and back
                            bracketTokens.PopFirst();
                            bracketTokens.PopLast();

                            //
                            IList<Dequeue<RawToken>> argList = GetArguments(bracketTokens);
                            IList<SyntaxTreeNode> nodes = new List<SyntaxTreeNode>();

                            foreach(Dequeue<RawToken> arg in argList)
                            {
                                SyntaxTreeNode item = null;

                                if (processType == BracketProcessType.AccessIndex)
                                {
                                    item = indexParentNode;
                                }
                                else if (processType == BracketProcessType.CreateArray)
                                {
                                    item = indexParentNode.CreateChildNode<ArrayItemNode>(null);
                                }

                                nodes.Add(Parse(item, arg, symbolTable));
                            }

                            returnNode = indexParentNode;
                        }
                        break;
                    #endregion

                    #region [TokenType.Semicolon]
                    case TokenType.Semicolon:
                        exitLoop = true;
                        break;
                    #endregion

                    #region [Default]
                    default:
                        errorListener.Report("ST0006", "Token type not handled: " + token.Content, token.Index.Item1, token.Index.Item2, token);
                        break;
                    #endregion
                }

                if (exitLoop)
                {
                    break;
                }
            }

            // Check wether there are tokens which should be ordered with the shunting-yard
            // For example (1 == 2 / 3)...
            if (treeNodeList.Count > 0)
            {
                // Build the SyntaxTree nodes from the tokens-list
                int openTerms = tokens.OfType<ParenthesisNode>().Where(Item => Item.Type == BracketType.Open).ToList().Count;
                int closeTerms = tokens.OfType<ParenthesisNode>().Where(Item => Item.Type == BracketType.Close).ToList().Count;

                if (openTerms > closeTerms)
                {
                    errorListener.Report("ST0007", "Syntax-Error: near line: . Missing ): " + treeNodeList.First().Token.Content, treeNodeList.First().Token.Index.Item1, treeNodeList.First().Token.Index.Item2, treeNodeList.First().Token);
                }
                else if (openTerms < closeTerms)
                {
                    errorListener.Report("ST0008", "Syntax-Error: near line: . Missing (: " + treeNodeList.First().Token.Content, treeNodeList.First().Token.Index.Item1, treeNodeList.First().Token.Index.Item2, treeNodeList.First().Token);
                }

                // Reorder tokens with the shunting-yard algorithm
                ShuntingYard yard = new ShuntingYard(treeNodeList);
                List<SyntaxTreeNode> postFixTokenized = yard.Execute().ToList();

                // Parse Post-Fix to Binary tree of Tokenized-Elements
                Stack<SyntaxTreeNode> tempStack = new Stack<SyntaxTreeNode>();
                SyntaxTreeNode lastNode = null;

                foreach (SyntaxTreeNode orderedToken in postFixTokenized)
                {
                    if (!ShuntingYard.TokenIsOperator(orderedToken))
                    {
                        tempStack.Push(orderedToken);
                    }
                    else
                    {
                        var firstNode = tempStack.Pop();
                        orderedToken.Children.Enqueue(firstNode);
                        firstNode.ParentNode = orderedToken;

                        var secondNode = tempStack.Pop();
                        orderedToken.Children.Enqueue(secondNode);
                        secondNode.ParentNode = orderedToken;

                        tempStack.Push(orderedToken);
                    }

                    lastNode = orderedToken;
                }

                parent.Children.Enqueue(lastNode);
                lastNode.ParentNode = parent;
                returnNode = parent;
            }

            return returnNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private bool ShouldBeProperty(SyntaxTreeNode parent)
        {
            if (parent is CallMethodNode)
            {
                return true;
            }
            else if (parent is AccessorNode)
            {
                return true;
            }
            else if (parent is CallFunctionNode)
            {
                return true;
            }
            else if (parent is PropertyAccessorNode)
            {
                if (parent.FindChildrenOfType<SetNode>().Count == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (parent is VariableNode)
            {
                if (parent.FindChildrenOfType<SetNode>().Count == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public SyntaxTreeNode ParseFunctionCall(SyntaxTreeNode parent, RawToken varToken, Dequeue<RawToken> tokens)
        {
            return null;
        }

        public SyntaxTreeNode ParseVariableDecl(SyntaxTreeNode parent, RawToken varToken, Dequeue<RawToken> tokens)
        {
            return null;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get every token between two tokens
        /// </summary>
        /// <param name="tokens">Token-List</param>
        /// <param name="openToken"></param>
        /// <param name="closeToken"></param>
        /// <returns>List of new tokens</returns>
        private Dequeue<RawToken> GetInnerTokens(Dequeue<RawToken> tokens, TokenType openToken, TokenType closeToken)
        {
            Dequeue<RawToken> returnList = new Dequeue<RawToken>();

            int bracketCounting = 0;
            while (tokens.Count > 0)
            {
                RawToken token = tokens.PopFirst();

                if (token.Type == openToken)
                {
                    bracketCounting++;
                }
                else if (token.Type == closeToken)
                {
                    bracketCounting--;
                }

                returnList.PushBack(token);

                // Remove added token
                if (bracketCounting == 0)
                {
                    break;
                }
            }

            return returnList;
        }

        /// <summary>
        /// Get all token until a specific token appears
        /// </summary>
        /// <param name="tokens">List of tokens</param>
        /// <param name="untilToken">Stop token</param>
        /// <returns>New list of tokens</returns>
        private Dequeue<RawToken> GetUntilTokens(Dequeue<RawToken> tokens, TokenType untilToken)
        {
            Dequeue<RawToken> returnList = new Dequeue<RawToken>();

            while (tokens.Count > 0)
            {
                RawToken token = tokens.PopFirst();

                returnList.PushBack(token);

                if (token.Type == untilToken)
                {
                    break;
                }
            }

            return returnList;
        }

        /// <summary>
        /// Get a list with all argumetns
        /// </summary>
        /// <param name="tokens">Token List</param>
        /// <returns></returns>
        private IList<Dequeue<RawToken>> GetArguments(Dequeue<RawToken> tokens)
        {
            IList<Dequeue<RawToken>> returnValue = new List<Dequeue<RawToken>>();
            int bracketCounter = 0;
            Dequeue<RawToken> currentList = new Dequeue<RawToken>();

            while (tokens.Count > 0)
            {
                RawToken currentToken = tokens.PopFirst();

                if (currentToken.Type == TokenType.OpenParenthesis || currentToken.Type == TokenType.OpenBracket || currentToken.Type == TokenType.OpenBraceToken)
                {
                    bracketCounter++;
                    currentList.PushBack(currentToken);
                }
                else if (currentToken.Type == TokenType.CloseParenthesis || currentToken.Type == TokenType.CloseBracket || currentToken.Type == TokenType.CloseBraceToken)
                {
                    bracketCounter--;
                    currentList.PushBack(currentToken);
                }
                else if (currentToken.Type == TokenType.Comma && bracketCounter == 0)
                {
                    if (currentList.Count == 0)
                    {
                        errorListener.Report("ST0009", "Token type not handled: " + currentToken.Content, currentToken.Index.Item1, currentToken.Index.Item2, currentToken);
                    }

                    returnValue.Add(currentList);
                    currentList = new Dequeue<RawToken>();
                }
                else
                {
                    currentList.PushBack(currentToken);
                }
            }

            if (currentList.Count > 0)
            {
                returnValue.Add(currentList);
            }

            return returnValue;
        }
        #endregion

        #region Public Member
        /// <summary>
        /// Entry-Point of the script (fragment)
        /// </summary>
        internal EntryPointNode EntryPoint
        {
            get { return entryPoint; }
        }
        #endregion
    }
}
