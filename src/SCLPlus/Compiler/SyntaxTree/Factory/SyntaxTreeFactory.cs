﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Core SyntaxTreeNode factory. In this class all RawTokens will be turned into SyntaxTreeNodes</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Syntax tree factory has the responsability to create all SyntaxTreeNodes by using Factory Tokens
    /// </summary>
    internal class SyntaxTreeFactory
    {
        #region Singleton
        private static readonly SyntaxTreeFactory singleton = new SyntaxTreeFactory();

        /// <summary>
        /// Singleton access to recude instance creation, because the factory will not change at runtime
        /// </summary>
        internal static SyntaxTreeFactory Singleton
        {
            get { return SyntaxTreeFactory.singleton; }
        }
        #endregion

        #region Private Member
        private IDictionary<string, StaticFactoryToken> staticTokens;
        private IDictionary<TokenType, DynamicFactoryToken> dynamicTokens;
        #endregion

        #region Constructor
        /// <summary>
        /// Create syntact tree factory
        /// </summary>
        private SyntaxTreeFactory()
        {
            // Create list of static factory tokens
            staticTokens = new Dictionary<string, StaticFactoryToken>();

            // Add all static factory tokens once
            staticTokens.Add(".", new StaticFactoryToken(".", TokenType.Dot));
            staticTokens.Add(";", new StaticFactoryToken(";", TokenType.Semicolon));
            staticTokens.Add(",", new StaticFactoryToken(",", TokenType.Comma));
            staticTokens.Add(":", new StaticFactoryToken(":", TokenType.Colon));
            staticTokens.Add("::", new StaticFactoryToken("::", TokenType.DoubleColon));
            staticTokens.Add(Environment.NewLine, new StaticFactoryToken(Environment.NewLine, TokenType.NewLine));
            staticTokens.Add("\n", new StaticFactoryToken("\n", TokenType.NewLine));

            staticTokens.Add("+", new StaticFactoryToken("+", TokenType.Add));
            staticTokens.Add("-", new StaticFactoryToken("-", TokenType.Subtract));
            staticTokens.Add("*", new StaticFactoryToken("*", TokenType.Multiply));
            staticTokens.Add("/", new StaticFactoryToken("/", TokenType.Devide));
            staticTokens.Add("=", new StaticFactoryToken("=", TokenType.Set));

            staticTokens.Add("==", new StaticFactoryToken("==", TokenType.Equal));
            staticTokens.Add("!=", new StaticFactoryToken("!=", TokenType.Unequal));
            staticTokens.Add(">", new StaticFactoryToken(">", TokenType.Greater));
            staticTokens.Add("<", new StaticFactoryToken("<", TokenType.Smaller));
            staticTokens.Add(">=", new StaticFactoryToken(">=", TokenType.GreaterEqual));
            staticTokens.Add("<=", new StaticFactoryToken("<=", TokenType.SmallerEqual));

            staticTokens.Add("&&", new StaticFactoryToken("&&", TokenType.And));
            staticTokens.Add("||", new StaticFactoryToken("||", TokenType.Or));

            staticTokens.Add("(", new StaticFactoryToken("(", TokenType.OpenParenthesis));
            staticTokens.Add(")", new StaticFactoryToken(")", TokenType.CloseParenthesis));
            staticTokens.Add("[", new StaticFactoryToken("[", TokenType.OpenBracket));
            staticTokens.Add("]", new StaticFactoryToken("]", TokenType.CloseBracket));
            staticTokens.Add("{", new StaticFactoryToken("{", TokenType.OpenBraceToken));
            staticTokens.Add("}", new StaticFactoryToken("}", TokenType.CloseBraceToken));

            staticTokens.Add("import", new StaticFactoryToken("import", TokenType.KeywordImport));
            staticTokens.Add("var", new StaticFactoryToken("var", TokenType.KeywordDeclVar));
            staticTokens.Add("new", new StaticFactoryToken("new", TokenType.KeywordNew));
            staticTokens.Add("function", new StaticFactoryToken("function", TokenType.KeywordDeclFunc));
            staticTokens.Add("return", new StaticFactoryToken("return", TokenType.KeywordReturn));

            // Create list of dynamic factory tokens
            dynamicTokens = new Dictionary<TokenType, DynamicFactoryToken>();

            dynamicTokens.Add(TokenType.Constant, new DynamicFactoryToken(TokenType.Constant));
            dynamicTokens.Add(TokenType.Name, new DynamicFactoryToken(TokenType.Name));
        }
        #endregion

        #region Public Member
        /// <summary>
        /// List with all dynamic token factories
        /// </summary>
        internal IDictionary<TokenType, DynamicFactoryToken> DynamicTokens
        {
            get { return dynamicTokens; }
        }

        /// <summary>
        /// List with all static token factories
        /// </summary>
        internal IDictionary<string, StaticFactoryToken> StaticTokens
        {
            get { return staticTokens; }
        }
        #endregion
    }
}
