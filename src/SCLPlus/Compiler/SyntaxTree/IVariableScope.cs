﻿
// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Scropt interface</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Interface which must be implemented by all SyntaxTreeNodes which are used for scoping declaration
    /// </summary>
    public interface IScopeNode
    {
        // List with all variables
        SymbolTable SymbolTable { get; }
    }
}
