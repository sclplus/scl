﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>IL-Code-Compiler</summary>

using SCLPlus.Collections.Generic;
using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Compiles a syntax tree to IL-Code
    /// </summary>
    public class ILCompiler : ICompiler
    {
        #region Private Member
        private IDictionary<string, DeclareFunctionNode> functions;
        private IErrorListener errorListener;
        #endregion

        #region Constructor
        /// <summary>
        /// Create compiler
        /// </summary>
        /// <param name="errorListener">Error listener instance</param>
        public ILCompiler(IErrorListener errorListener)
        {
            functions = new Dictionary<string, DeclareFunctionNode>();
            this.errorListener = errorListener;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Compile to IL-Code
        /// </summary>
        /// <param name="node">TreeNode instnace</param>
        /// <returns>Stream with the compiled code</returns>
        public System.IO.Stream Compile(SyntaxTreeNode node)
        {
            // Preprocessing
            // Get all function declaration
            foreach (var funcDecl in node.FindChildrenOfType<DeclareFunctionNode>())
            {
                if (functions.ContainsKey(funcDecl.Token.Content))
                {
                    errorListener.Report("C0001", "Function already exists: " + funcDecl.Token.Content, funcDecl.Token.Index.Item1, funcDecl.Token.Index.Item2, funcDecl.Token);
                }
                else
                {
                    functions.Add(funcDecl.Token.Content, funcDecl);
                }
            }

            StringBuilder strBuilder = new StringBuilder();

            strBuilder.AppendLine("// SCL+ program");
            //strBuilder.AppendLine();
            //strBuilder.AppendLine(".assembly extern mscorlib {}");
            //strBuilder.AppendLine();
            //strBuilder.AppendLine(".assembly Methods");
            //strBuilder.AppendLine("{");
            //strBuilder.AppendLine("    .ver 1:0:1:0");
            //strBuilder.AppendLine("}");
            strBuilder.AppendLine();
            strBuilder.AppendLine(".module Methods.exe");
            strBuilder.AppendLine();

            // ===================================================
            // Get all variables in the scope
            strBuilder.Append(".locals (");

            StringBuilder localBuilder = new StringBuilder();
            foreach (DeclareVariableNode declVarNode in node.FindChildrenOfType<DeclareVariableNode>())
            {
                if (localBuilder.Length > 0)
                {
                    localBuilder.Append(", ");
                }

                localBuilder.Append(declVarNode.Token.Content);
            }
            strBuilder.Append(localBuilder.ToString());
            strBuilder.AppendLine(")");
            // ===================================================

            // Compile to output
            foreach (var child in node.Children)
            {
                Compile(strBuilder, child, 0);
            }

            // Return code
            return new System.IO.MemoryStream(Encoding.UTF8.GetBytes(strBuilder.ToString()));
        }
        #endregion

        #region Private Member
        private void Compile(StringBuilder strBuilder, SyntaxTreeNode node, int intendend)
        {
            string intendendStr = new string('\t', intendend);

            switch (node.NodeType)
            {
                #region [SyntaxNodeType.FuncDecl]
                case SyntaxNodeType.FuncDecl:
                    {
                        // Compile function deklaration
                        CompileFunction(strBuilder, (DeclareFunctionNode)node);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.VariableDecl]
                case SyntaxNodeType.VariableDecl:
                    {
                        if (node.Children.Count > 0)
                        {
                            CompileExpression(strBuilder, node, intendend);

                            // Pop expression back to variable
                            strBuilder.AppendLine(intendendStr + "stloc." + node.Token.Content);
                        }
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.Return]
                case SyntaxNodeType.Return:
                    {
                        if (node.Children.Count > 0)
                        {
                            CompileExpression(strBuilder, node, intendend);
                            strBuilder.AppendLine(intendendStr + "pop");
                        }

                        strBuilder.AppendLine(intendendStr + "ret");
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.New]
                case SyntaxNodeType.New:
                    {
                        Compile(strBuilder, node.Children.First(), intendend);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.CreateInstance]
                case SyntaxNodeType.CreateInstance:
                    {
                        StringBuilder argBuilder = new StringBuilder();
                        string classPath = "";

                        // ======================================================================
                        CreateInstanceNode createInstance = (node as CreateInstanceNode);

                        if (createInstance.Class is ClrClass)
                        {
                            classPath = (createInstance.Class as ClrClass).GetPath();
                            argBuilder.Append(intendendStr + "newobj instance " + classPath + "(");
                        }
                        // ======================================================================

                        bool argsFound = false;

                        int i = 0;
                        foreach (var args in node.FindChildrenOfType<ArgumentNode>())
                        {
                            // Parse as expression and push result on the argument stack
                            CompileExpression(strBuilder, args, intendend);
                            strBuilder.AppendLine((new string('\t', intendend)) + "ldarg." + i.ToString());

                            if (argsFound)
                            {
                                argBuilder.Append(", ");
                            }

                            argBuilder.Append("_dummy_cip" + i.ToString());

                            argsFound = true;
                            i++;
                        }

                        argBuilder.Append(")");

                        // Call the function
                        strBuilder.AppendLine(argBuilder.ToString());
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.CallMethod]
                case SyntaxNodeType.CallMethod:
                    {
                        StringBuilder argBuilder = new StringBuilder();
                        argBuilder.Append(intendendStr + "call.m " + (node as SyntaxTreeNode).Token.Content + "(");
                        bool argsFound = false;

                        int i = 0;
                        foreach (var args in node.FindChildrenOfType<ArgumentNode>())
                        {
                            // Parse as expression and push result on the argument stack
                            CompileExpression(strBuilder, args, intendend);
                            strBuilder.AppendLine((new string('\t', intendend)) + "ldarg." + i.ToString());

                            if (argsFound)
                            {
                                argBuilder.Append(", ");
                            }

                            argBuilder.Append("_dummy_mp" + i.ToString());

                            argsFound = true;
                            i++;
                        }

                        argBuilder.Append(")");

                        // Call the function
                        strBuilder.AppendLine(argBuilder.ToString());

                        // Parse accessors
                        ParseAccessors(strBuilder, node, intendend);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.CallClass]
                case SyntaxNodeType.CallClass:
                    {
                        string classPath = "";

                        CallClassNode ccNode = (node as CallClassNode);

                        if (ccNode.Class is ClrClass)
                        {
                            classPath = (ccNode.Class as ClrClass).GetPath();
                            strBuilder.AppendLine(intendendStr + "oldclass.ext " + classPath);
                        }

                        Compile(strBuilder, ccNode.Children.First(), intendend);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.FuncCall]
                case SyntaxNodeType.FuncCall:
                    {
                        StringBuilder argBuilder = new StringBuilder();
                        argBuilder.Append(intendendStr + "call.f " + (node as SyntaxTreeNode).Token.Content + "(");
                        bool argsFound = false;

                        int i = 0;
                        foreach (var args in node.FindChildrenOfType<ArgumentNode>())
                        {
                            // Parse as expression and push result on the argument stack
                            CompileExpression(strBuilder, args, intendend);
                            strBuilder.AppendLine((new string('\t', intendend)) + "ldarg." + i.ToString());

                            if (argsFound)
                            {
                                argBuilder.Append(", ");
                            }

                            argBuilder.Append("_dummy_fp" + i.ToString());

                            argsFound = true;
                            i++;
                        }

                        argBuilder.Append(")");

                        // Call the function
                        strBuilder.AppendLine(argBuilder.ToString());

                        // Parse accessors
                        ParseAccessors(strBuilder, node, intendend);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.Constant]
                case SyntaxNodeType.Constant:
                    var constNode = (node as ConstantNode);

                    if (constNode.DataType == DataType.Null)
                    {
                        strBuilder.AppendLine("ldnull");
                    }
                    else
                    {
                        var type = GetConstantTypeStr(constNode);
                        string val = node.Token.Content;

                        if (val.EndsWith("i") || val.EndsWith("l") || val.EndsWith("f") || val.EndsWith("d"))
                        {
                            val = val.Substring(0, val.Length - 1);
                        }

                        strBuilder.AppendLine(intendendStr + "ldc." + type + " " + val);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.Set]
                case SyntaxNodeType.Set:
                    {
                        // Compile values to be set
                        CompileExpression(strBuilder, node, intendend);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.Accessor]
                case SyntaxNodeType.Accessor:
                    {
                        // Compile values to be set
                        Compile(strBuilder, node.Children.Peek(), intendend);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.PropertyAccessor]
                case SyntaxNodeType.PropertyAccessor:
                    {
                        if (node.FindChildrenOfType<SetNode>().Count > 0)
                        {
                            // Compile value to be set
                            Compile(strBuilder, node.Children.Peek(), intendend);

                            strBuilder.AppendLine(intendendStr + "stfld." + node.Token.Content);
                        }

                        else
                        {
                            strBuilder.AppendLine(intendendStr + "ldfld." + node.Token.Content);

                            var children = node.FindChildrenOfType<AccessorNode>();
                            if (children.Count == 1)
                            {
                                Compile(strBuilder, node.Children.Peek(), intendend);
                            }
                            else
                            {
                                // Parse accessors
                                ParseAccessors(strBuilder, node, intendend);
                            }
                        }
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.CreateArray]
                case SyntaxNodeType.CreateArray:
                    {
                        // Compile values to be set
                        strBuilder.AppendLine(intendendStr + "newarr");

                        foreach (SyntaxTreeNode innerNode in node.Children)
                        {
                            Compile(strBuilder, innerNode.Children.Peek(), intendend);
                            strBuilder.AppendLine(intendendStr + "addelem");
                        }
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.IndexAccess]
                case SyntaxNodeType.IndexAccess:
                    {
                        // Compile values to be set
                        if (node.Children.Count > 0)
                        {
                            foreach (var child in node.Children)
                            {
                                if ((child is SetNode) == false && (child is AccessorNode) == false && (child is IndexAccessNode) == false)
                                {
                                    Compile(strBuilder, node.Children.Peek(), intendend);
                                }
                            }
                        }

                        if (node.FindChildrenOfType<SetNode>().Count > 0)
                        {
                            // Compile value to be set
                            Compile(strBuilder, node.FindChildrenOfType<SetNode>().First(), intendend);

                            strBuilder.AppendLine(intendendStr + "stelem");
                        }
                        else
                        {
                            strBuilder.AppendLine(intendendStr + "ldelem");

                            // Parse accessors
                            ParseAccessors(strBuilder, node, intendend);
                        }
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.Variable]
                case SyntaxNodeType.Variable:
                    if (node.FindChildrenOfType<SetNode>().Count == 0)
                    {
                        // Push var to the stack
                        strBuilder.AppendLine(intendendStr + "ldloc." + node.Token.Content);

                        // Parse accessors
                        ParseAccessors(strBuilder, node, intendend);
                    }
                    else if (node.FindChildrenOfType<SetNode>().Count > 0)
                    {
                        // Compile value to be set
                        Compile(strBuilder, node.Children.Peek(), intendend);

                        // set values
                        strBuilder.AppendLine(intendendStr + "stloc." + node.Token.Content);
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.Operator]
                case SyntaxNodeType.Operator:
                    {
                        switch ((node as OperatorNode).OperatorType)
                        {
                            case OperatorType.Add:
                                strBuilder.AppendLine(intendendStr + "add");
                                break;
                            case OperatorType.Sub:
                                strBuilder.AppendLine(intendendStr + "sub");
                                break;
                            case OperatorType.Mul:
                                strBuilder.AppendLine(intendendStr + "mul");
                                break;
                            case OperatorType.Div:
                                strBuilder.AppendLine(intendendStr + "div");
                                break;
                            case OperatorType.Equal:
                                strBuilder.AppendLine(intendendStr + "eq");
                                break;
                            case OperatorType.Unequal:
                                strBuilder.AppendLine(intendendStr + "ueq");
                                break;
                            case OperatorType.Greater:
                                strBuilder.AppendLine(intendendStr + "gt");
                                break;
                            case OperatorType.Smaller:
                                strBuilder.AppendLine(intendendStr + "sm");
                                break;
                            case OperatorType.GreaterEqual:
                                strBuilder.AppendLine(intendendStr + "gteq");
                                break;
                            case OperatorType.SmallerEqual:
                                strBuilder.AppendLine(intendendStr + "smeq");
                                break;
                            case OperatorType.And:
                                strBuilder.AppendLine(intendendStr + "and");
                                break;
                            case OperatorType.Or:
                                strBuilder.AppendLine(intendendStr + "or");
                                break;
                        }
                    }
                    break;
                #endregion

                #region [SyntaxNodeType.AnonymObject]
                case SyntaxNodeType.AnonymousObject:
                    {
                        CompileAnoymObject(strBuilder, node, intendend);
                    }
                    break;

                case SyntaxNodeType.AnonymProperty:
                    {
                        // Compile value to be set
                        Compile(strBuilder, node.Children.Peek(), intendend);

                        strBuilder.AppendLine(intendendStr + "stloc." + node.Token.Content);
                    }
                    break;
                #endregion
            }
        }

        public void ParseAccessors(StringBuilder strBuilder, SyntaxTreeNode node, int intendend)
        {
            if (node.FindChildrenOfType<AccessorNode>().Count == 1)
            {
                Compile(strBuilder, node.FindChildrenOfType<AccessorNode>().First(), intendend);
            }
            else if (node.FindChildrenOfType<IndexAccessNode>().Count > 0)
            {
                Compile(strBuilder, node.FindChildrenOfType<IndexAccessNode>().First(), intendend);
            }
        }

        #region [CompileExpression]
        /// <summary>
        /// Compile expressions like ... = 1 - 2 + 3 == 2
        /// </summary>
        /// <param name="strBuilder"></param>
        /// <param name="node"></param>
        /// <param name="intendend"></param>
        private void CompileExpression(StringBuilder strBuilder, SyntaxTreeNode node, int intendend)
        {
            Dequeue<SyntaxTreeNode> postFixQueue = new Dequeue<SyntaxTreeNode>();

            // Parse the tree to postfix notation
            List<SyntaxTreeNode> tempList = node.Children.ToList();

            while (tempList.Count > 0)
            {
                List<SyntaxTreeNode> innerList = new List<SyntaxTreeNode>();

                foreach (SyntaxTreeNode tempNode in tempList.Where(Item => Item.NodeType != SyntaxNodeType.Operator).ToList())
                {
                    postFixQueue.PushFront(tempNode);
                }

                foreach (SyntaxTreeNode tempNode in tempList.Where(Item => Item.NodeType == SyntaxNodeType.Operator).ToList())
                {
                    postFixQueue.PushFront(tempNode);

                    innerList.AddRange(tempNode.Children);
                }

                tempList.Clear();
                tempList = innerList;
            }

            // Compile
            while (postFixQueue.Count > 0)
            {
                var qNode = postFixQueue.PopFirst();

                // Compile tree element
                Compile(strBuilder, qNode, intendend);
            }
        }
        #endregion

        #region [CompileFunction]
        /// <summary>
        /// Compile function body
        /// </summary>
        /// <param name="strBuilder"></param>
        /// <param name="node"></param>
        private void CompileFunction(StringBuilder strBuilder, DeclareFunctionNode node)
        {
            // Function header
            strBuilder.Append(".function " + node.Token.Content);

            // ===================================================================================
            // Argumetns
            strBuilder.Append("(");

            StringBuilder argBuilder = new StringBuilder();
            foreach (ArgumentNode arg in node.FindChildrenOfType<ArgumentNode>())
            {
                if (argBuilder.Length > 0)
                {
                    argBuilder.Append(", ");
                }

                argBuilder.Append(arg.Token.Content);
            }
            strBuilder.Append(argBuilder.ToString());
            strBuilder.AppendLine(")");
            // ===================================================================================

            // ===================================================================================
            // Body
            strBuilder.AppendLine("{");

            // ===================================================
            // Get all variables in the scope
            strBuilder.Append("\t.locals (");

            StringBuilder localBuilder = new StringBuilder();
            foreach (VariableSymbol symbol in node.SymbolTable.FindSymbols<VariableSymbol>())
            {
                if (localBuilder.Length > 0)
                {
                    localBuilder.Append(", ");
                }

                localBuilder.Append(symbol.TreeNode.Token.Content);
            }
            strBuilder.Append(localBuilder.ToString());
            strBuilder.AppendLine(")");
            // ===================================================

            // Set stack size
            strBuilder.AppendLine("\t.maxstack (2)");

            // Parse inner
            foreach (var child in node.Children)
            {
                Compile(strBuilder, child, 1);
            }

            strBuilder.AppendLine("}");
            // ===================================================================================
        }
        #endregion

        #region [CompileAnoymObject]
        /// <summary>
        /// Compile function body
        /// </summary>
        /// <param name="strBuilder"></param>
        /// <param name="node"></param>
        private void CompileAnoymObject(StringBuilder strBuilder, SyntaxTreeNode node, int intendend)
        {
            string intendendStr = new string('\t', intendend);

            // Function header
            strBuilder.AppendLine(intendendStr + ".anonym.class");

            // ===================================================================================
            // Body
            strBuilder.AppendLine(intendendStr + "{");

            // ===================================================
            // Get all variables in the scope
            strBuilder.Append(intendendStr + "\t.locals (");

            StringBuilder localBuilder = new StringBuilder();
            foreach (AnonymObjectProperty declVarNode in node.FindChildrenOfType<AnonymObjectProperty>())
            {
                if (localBuilder.Length > 0)
                {
                    localBuilder.Append(", ");
                }

                localBuilder.Append(declVarNode.Token.Content);
            }
            strBuilder.Append(localBuilder.ToString());
            strBuilder.AppendLine(")");
            // ===================================================

            // Parse inner
            foreach (var child in node.Children)
            {
                Compile(strBuilder, child, intendend + 1);
            }

            strBuilder.AppendLine(intendendStr + "}");
            // ===================================================================================
        }
        #endregion

        #region [GetConstantTypeStr]
        /// <summary>
        /// Get the type as a string
        /// </summary>
        /// <param name="node">Node instnace</param>
        /// <returns>Type as string</returns>
        public string GetConstantTypeStr(ConstantNode node)
        {
            switch (node.DataType)
            {
                case DataType.None:
                case DataType.Null:
                    break;

                case DataType.Boolean:
                    return "i2";

                case DataType.Int32:
                    return "i4";

                case DataType.Int64:
                    return "i8";

                case DataType.Float32:
                    return "r4";

                case DataType.Float64:
                    return "r8";

                case DataType.Str:
                    return "str";

                default:
                    return "";
            }

            return "";
        }
        #endregion

        #endregion

        #region Public Member

        #endregion
    }
}
