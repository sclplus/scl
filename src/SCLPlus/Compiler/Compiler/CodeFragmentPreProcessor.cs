﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Code-Fragment Pre-Processor</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Code fragment pre processor, prepares code fragments
    /// </summary>
    internal class CodeFragmentPreProcessor
    {
        #region Public Methods
        /// <summary>
        /// Preprocess a code fragment for compiling.
        /// </summary>
        /// <param name="fragment">Code fragments</param>
        /// <returns>Instance of a new Code-Fragment</returns>
        public static CodeFragment Prepare(CodeFragment fragment)
        {
            CodeFragment returnValue = new CodeFragment();


            StringBuilder builder = new StringBuilder();
            //builder.AppendLine("function " + fragment.Name + " () {");

            returnValue.Type = fragment.Type;

            if (fragment.Type == CodeFragmentType.BooleanExpress)
            {
                // Set fragment specific code
               // builder.AppendLine("var __rt = " + fragment.Code + ";");

                // Return the boolean value
                //builder.AppendLine("return __rt;");
            }
            else if (fragment.Type == CodeFragmentType.Command)
            {
                // Set fragment specific code
                builder.AppendLine(fragment.Code);
            }

            //builder.AppendLine("}");
            returnValue.Code = fragment.Code; // builder.ToString();

            return returnValue;
        }
        #endregion
    }
}
