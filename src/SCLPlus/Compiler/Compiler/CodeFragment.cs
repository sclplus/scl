﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Code fragment container</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Code fragment container
    /// </summary>
    internal class CodeFragment
    {
        /// <summary>
        /// Code of the fragment
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Name of the code fragment
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Type of the code fragment
        /// </summary>
        public CodeFragmentType Type
        {
            get;
            set;
        }
    }
}
