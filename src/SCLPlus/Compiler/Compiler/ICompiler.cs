﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Compiler interface</summary>

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Compiler interface
    /// </summary>
    public interface ICompiler
    {
        /// <summary>
        /// Compile method impl.
        /// </summary>
        /// <param name="node">Node instance (for exmaple EtnryPointNode)</param>
        /// <returns>Stream with the compiled code</returns>
        Stream Compile(SyntaxTreeNode node);
    }
}
