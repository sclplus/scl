﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Result object returned by the compiler
    /// </summary>
    public class CompilerResult
    {
        /// <summary>
        /// List with entry points containing the syntax tree
        /// </summary>
        public IList<EntryPointNode> EntryPoints
        {
            get;
            internal set;
        }

        /// <summary>
        /// Stream containing compiled code
        /// </summary>
        public Stream Stream
        {
            get;
            set;
        }
    }
}
