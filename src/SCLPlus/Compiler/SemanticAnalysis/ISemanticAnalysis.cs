﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Interface which must be implemented in all syntax tree nodes, to proof, wehter theire semantic is correct
    /// </summary>
    public interface ISemanticAnalysis
    {
        /// <summary>
        /// Proof, wether the current syntax tree semantic is valid
        /// </summary>
        void CheckSemantic();
    }
}
