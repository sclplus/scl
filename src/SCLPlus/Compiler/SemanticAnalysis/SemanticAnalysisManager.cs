﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Contains all methods and logic for semantic syntaxtree analysis/summary>#

using SCLPlus.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Syntax-tree analyser, proof wether all parent-child conditions are correct
    /// </summary>
    public class SemanticAnalysisManager
    {
        #region Private Memner
        private IErrorListener errorListener;
        #endregion

        #region Constructor
        /// <summary>
        /// Create the manager and init (load rules)
        /// </summary>
        /// <param name="errorListener">Error listener for error reporting</param>
        public SemanticAnalysisManager(IErrorListener errorListener)
        {
            this.errorListener = errorListener;
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        public void Analyse(IList<SyntaxTreeNode> list)
        {
            foreach (var node in list)
            {
                try
                {
                    // check the current semantic
                    node.CheckSemantic();
                }
                catch (Exception ex)
                {
                    // Report error
                    errorListener.Report("SA0001", ex.Message, node.Token.Index.Item1, node.Token.Index.Item2, node.Token);
                }
            }
        }
        #endregion
    }
}
