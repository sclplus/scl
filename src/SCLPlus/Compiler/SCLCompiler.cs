﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Simulation Control Language compiler. It compiles a script to scl+ runtime executeable code</summary>

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Compiler
{
    /// <summary>
    /// Class containing all core compiler methods, which will be usable for everyone using this assembly
    /// </summary>
    public class SCLCompiler
    {
        #region Private Member
        private IDictionary<string, CodeFragment> codeFragments;
        private IErrorListener errorListener;
        #endregion

        #region Constructor
        /// <summary>
        /// Create new compiler instance
        /// </summary>
        public SCLCompiler(IErrorListener errorListener)
        {
            codeFragments = new Dictionary<string, CodeFragment>();
            this.errorListener = errorListener;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Compile a single code-fragment
        /// </summary>
        /// <param name="fragment">Code-Fragment content</param>
        /// <returns>Stream with the compiled code</returns>
        private CompilerResult CompileCodeFragment(CodeFragment fragment)
        {
            // Preprocess fragment
            fragment = CodeFragmentPreProcessor.Prepare(fragment);

            // Tokenize the code
            ParserConfiguration parser = new ParserConfiguration();
            Tokenizer tokenizer = new Tokenizer(parser, errorListener);

            // let the tokenizer parse the code
            tokenizer.Parse(fragment.Code);

            // Create syntax tree
            SyntaxTreeBuilder stb = new SyntaxTreeBuilder(parser, tokenizer.Tokens, errorListener);
            var ep = stb.Build();

            // Semantic analyse
            SemanticAnalysisManager sa = new SemanticAnalysisManager(errorListener);
            //sa.Analyse(ep);

            // Compile
            ICompiler compiler = new ILCompiler(errorListener);
            var compilerRes = compiler.Compile(ep);

            var returnValue = new CompilerResult() { Stream = compilerRes, EntryPoints = new List<EntryPointNode>() };
            returnValue.EntryPoints.Add(ep);

            return returnValue;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Add a code fragment to the code which will be compiled
        /// </summary>
        /// <param name="code">Code</param>
        /// <param name="name">Name of the code. The name must be unique</param>
        /// <param name="type">Type of the code fragment</param>
        /// <param name="overwriteExisting">Defines, wether an existing code fragment which a specific name should be overwritten if it already exists</param>
        public void AddCodeFragment(string code, string name, CodeFragmentType type, bool overwriteExisting = false)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ArgumentNullException("code", "Code can not be null or whitespace");
            }
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("name", "Name can not be null or whitespace");
            }

            if (type == CodeFragmentType.Command)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("function " + name + "() {");
                builder.AppendLine(code);
                builder.AppendLine("}");

                code = builder.ToString();
            }
            else if (type == CodeFragmentType.BooleanExpress)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("function " + name + "() {");
                builder.AppendLine("return " + code + ";");
                builder.AppendLine("}");

                code = builder.ToString();
            }

            CodeFragment fragment = new CodeFragment() { Code = code, Name = name, Type = type };

            if (!overwriteExisting && codeFragments.ContainsKey(name.ToLower()))
            {
                throw new Exception("Code fragment with the name: " + name + " already exsits. If you want to overwrite the fragment, set overwriteExisting to true");
            }
            else if (codeFragments.ContainsKey(name.ToLower()))
            {
                codeFragments[name.ToLower()] = fragment;
            }
            else if (!codeFragments.ContainsKey(name.ToLower()))
            {
                codeFragments.Add(name.ToLower(), fragment);
            }
        }

        /// <summary>
        /// Compile all code fragments to a single executable script
        /// </summary>
        /// <param name="useParallelCompiling">Defines, wether the compile should compile the code in parallel threads</param>
        /// <returns>Stream containing the script content</returns>
        public CompilerResult Compile(bool useParallelCompiling)
        {
            if (codeFragments.Count == 0)
            {
                throw new Exception("There must be a minimum of one code fragment to compile");
            }

            CompilerResult returnValue = new CompilerResult();
            returnValue.EntryPoints = new List<EntryPointNode>();
            returnValue.Stream = new MemoryStream();
            
            if (!useParallelCompiling)
            {
                foreach (var fr in codeFragments)
                {
                    var res = CompileCodeFragment(fr.Value);

                    // Append to returnvalue, so the compiled code will be complete
                    res.Stream.CopyTo(returnValue.Stream);
                    returnValue.EntryPoints.Add(res.EntryPoints.First());
                }
            }
            else
            {
                List<Task<CompilerResult>> tasks = new List<Task<CompilerResult>>();

                foreach (var fr in codeFragments)
                {
                    // Start async compiling
                    tasks.Add(Task<CompilerResult>.Factory.StartNew(() => CompileCodeFragment(fr.Value)));
                }

                // Wait until all tasks are finished
                Task.WaitAll(tasks.ToArray());

                // Append output tu returnValue
                foreach (var task in tasks)
                {
                    // Append to returnvalue, so the compiled code will be complete
                    task.Result.Stream.CopyTo(returnValue.Stream);
                    returnValue.EntryPoints.Add(task.Result.EntryPoints.First());
                }
            }

            return returnValue;
        }
        #endregion

        #region Public Member
        /// <summary>
        /// Error listener instance
        /// </summary>
        public IErrorListener ErrorListener
        {
            get { return errorListener; }
        }
        #endregion
    }
}
