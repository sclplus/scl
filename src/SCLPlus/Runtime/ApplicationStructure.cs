﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Application structure</summary>

using SCLPlus.Runtime.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    /// <summary>
    /// Signleton application structure
    /// </summary>
    internal class ApplicationStructure
    {
        #region Private Member

        #endregion

        #region Constructor
        /// <summary>
        /// Create app structure
        /// </summary>
        public ApplicationStructure()
        {
            Functions = new Dictionary<string, FunctionNode>();
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        #endregion

        #region Public Member
        /// <summary>
        /// App root
        /// </summary>
        public RootNode Root
        {
            get;
            set;
        }

        /// <summary>
        /// List with all functions
        /// </summary>
        public IDictionary<string, FunctionNode> Functions
        {
            get;
            private set;
        }
        #endregion
    }
}
