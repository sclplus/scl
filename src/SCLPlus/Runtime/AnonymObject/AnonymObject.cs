﻿using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    public class AnonymObject : DynamicObject
    {
        #region Private Member
        IDictionary<string, Variable> properties;
        #endregion

        #region Constructor
        /// <summary>
        /// Create new dynamic object
        /// </summary>
        public AnonymObject()
        {
            properties = new Dictionary<string, Variable>();
        }   
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        /// <summary>
        /// Add a property to the dynamic object
        /// </summary>
        /// <param name="name">Name of the property</param>
        /// <param name="value">Value of an object</param>
        internal void AddProperty(string name, Variable variable)
        {
            properties.Add(name, variable);
        }

        /// <summary>
        /// Get a property
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal Variable GetProperty(string name)
        {
            if (properties.ContainsKey(name))
            {
                return properties[name];
            }
            else
            {
                throw new Exception("Property does not exists: " + name);
            }
        }

        /// <summary>
        /// Set variable
        /// </summary>
        /// <param name="variable"></param>
        internal void SetProperty(Variable variable)
        {
            if (properties.ContainsKey(variable.Name))
            {
                properties[variable.Name] = variable;
            }
            else
            {
                throw new Exception("Property does not exists: " + variable.Name);
            }
        }

        /// <summary>
        /// Try to get a dynamic property
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (properties.ContainsKey(binder.Name))
            {
                result = properties[binder.Name].Value;
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Try to set a dynamic property
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (properties.ContainsKey(binder.Name))
            {
                properties[binder.Name].Value = value;
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Public Member

        #endregion
    }
}
