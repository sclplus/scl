﻿using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    internal struct StackItem
    {
        public object Value
        {
            get;
            set;
        }

        public DataType DataType
        {
            get;
            set;
        }
    }
}
