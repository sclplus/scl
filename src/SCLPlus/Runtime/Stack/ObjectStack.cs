﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    public class ObjectStack
    {
        #region Private Member
        private Stack<Variable> stack;
        #endregion

        #region Constructor
        /// <summary>
        /// Create new object stack
        /// </summary>
        public ObjectStack()
        {
            stack = new Stack<Variable>();
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        /// <summary>
        /// Push object to the front
        /// </summary>
        /// <param name="obj"></param>
        internal void Push(Variable obj)
        {
            stack.Push(obj);
        }

        /// <summary>
        /// Pop object from the front
        /// </summary>
        /// <returns></returns>
        internal Variable Pop()
        {
            return stack.Pop();
        }
        #endregion

        #region Public Member

        #endregion
    }
}
