﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Core runtime, executes the generated IL-Code</summary>

using SCLPlus.Runtime.Commands;
using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    /// <summary>
    /// SCL+ runtime core.
    /// </summary>
    public class SCLRuntime
    {
        #region Private Member
        private ApplicationStructure appStruct;
        private Scope rootScope;
        #endregion

        #region Constructor
        /// <summary>
        /// Create SCL-Runtime
        /// </summary>
        public SCLRuntime()
        {
            appStruct = new ApplicationStructure();

            // create basic scope
            rootScope = new Scope(null);

            SCLPlus.Lib.StdLib.StandardLibraryHelper.Init(this);
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        /// <summary>
        /// Execute the code
        /// </summary>
        public void Execute(Stream ilCode)
        {
            Parser parser = new Parser();
            parser.Parse(ilCode);

            // get the createt application structure and merge it
            foreach (var function in parser.AppStruct.Functions)
            {
                if (appStruct.Functions.ContainsKey(function.Key) == false)
                {
                    appStruct.Functions.Add(function.Key, function.Value);
                }
            }

            // Iterate over all commandchain nodes
            foreach (var node in parser.AppStruct.Root.Children)
            {
                // Execute commands on root level
                ExecuteCommand(node, rootScope);
            }
        }

        #region [Execute Command]
        /// <summary>
        /// Execute a single command, cann be classed recursive
        /// </summary>
        /// <param name="node">ChainNode instance (command)</param>
        /// <param name="scope">Current scope, in which the command is executed</param>
        internal void ExecuteCommand(CommandChainNode node, Scope scope)
        {
            // Define vars in the scope
            if (node is LocalsNode)
            {
                foreach (string varName in (node as LocalsNode).VariableDecl)
                {
                    scope.CreateVariable(varName);
                }
            }

            else if (node is CallFunctionNode)
            {
                Scope funcScope = new Scope(scope);
                var callFuncNode = (node as CallFunctionNode);

                if (appStruct.Functions.ContainsKey(callFuncNode.FunctionName) == false)
                {
                    throw new Exception("Function not found: " + callFuncNode.FunctionName);
                }

                var funcNode = appStruct.Functions[callFuncNode.FunctionName];

                foreach (string varName in funcNode.ArgumentDecl)
                {
                    try
                    {
                        var var = funcScope.CreateVariable(varName);

                        // Set argument values
                        var tempItem = scope.ArgumentStack.PopFirst();
                        var.DataType = tempItem.Item2.DataType;
                        var.Value = tempItem.Item2.Value;
                    }
                    catch
                    {
                        throw new Exception("Parameter count does not match for: " + funcNode.FunctionName + "; Expected " + funcNode.ArgumentDecl.Count.ToString() + " parameter");
                    }
                }
                
                foreach (var child in funcNode.Children)
                {
                    ExecuteCommand(child, funcScope);
                }
            }

            else if (node is CallMethodNode)
            {
                var method = (node as CallMethodNode);

                var obj = scope.Stack.Pop();

                var tmpArg = new SCLPlus.Collections.Generic.Dequeue<Tuple<int, StackItem>>();
                for (int i = 0; i < method.ParameterCount; i++)
                {
                    tmpArg.PushBack(scope.ArgumentStack.PopFirst());
                }

                Type[] typeList = tmpArg.Select(Item => (Item.Item2.Value == null ? null : Item.Item2.Value.GetType())).ToArray();

                if (obj.Value is Type)
                {
                    // Find clr method
                    var clrMethod = (obj.Value as Type).GetMethod(method.MethodName, typeList);

                    // Call clr method
                    var mResult = clrMethod.Invoke(obj.Value, tmpArg.Select(Item => Item.Item2.Value).ToArray());

                    //
                    scope.Stack.Push(mResult, Helper.GetDataTypeByObject(mResult));
                }
                else if (obj.Value != null)
                {
                    // Find clr method
                    var clrMethod = obj.Value.GetType().GetMethod(method.MethodName, typeList);

                    // Call clr method
                    var mResult = clrMethod.Invoke(obj.Value, tmpArg.Select(Item => Item.Item2.Value).ToArray());

                    //
                    scope.Stack.Push(mResult, Helper.GetDataTypeByObject(mResult));
                }
            }

            else if (node is PopNode)
            {
                // Write return-value to the stack
                var popedVal = scope.Stack.Pop();
                scope.ParentScope.Stack.Push(popedVal.Value, popedVal.DataType);
            }

            // return
            else if (node is ReturnNode)
            {
                // return from function
                return;
            }

            // AnonymObjectNode
            else if (node is AnonymObjectNode)
            {
                Scope aoScope = new Scope(scope);
                var aoNode = (node as AnonymObjectNode);

                // Create new dynamic object
                AnonymObject ao = new AnonymObject();
                //aoScope.ObjectStack.Push(ao);

                foreach (CommandChainNode child in aoNode.Children)
                {
                    ExecuteCommand(child, aoScope);
                }

                // If all properties are processed, add them as as property
                foreach (var var in aoScope.Vars)
                {
                    ao.AddProperty(var.Key, var.Value);
                }

                scope.Stack.Push(ao, Shared.DataType.AnonymObject);
            }

            // Constant handling
            // Push constant node to the stack
            else if (node is PushConstantNode)
            {
                var constNode = (node as PushConstantNode);
                scope.Stack.Push(constNode.ConstantValue, constNode.DataType);
            }

            // Variable pushing
            else if (node is PushVariableNode)
            {
                var var = scope.GetVariable((node as PushVariableNode).VariableName);
                scope.Stack.Push(var.Value, var.DataType);
            }

            else if (node is LoadExternClassNode)
            {
                var tmp = (node as LoadExternClassNode);
                scope.Stack.Push(tmp.ClassType, DataType.Object);
            }

            else if (node is CreateExternClassInstanceNode)
            {
                var tmp = (node as CreateExternClassInstanceNode);

                var tmpArg = new SCLPlus.Collections.Generic.Dequeue<Tuple<int, StackItem>>();
                for (int i = 0; i < tmp.ParameterCount; i++)
                {
                    tmpArg.PushBack(scope.ArgumentStack.PopFirst());
                }

                object[] _params = tmpArg.Select(Item => Item.Item2.Value).ToArray();

                var instance = Activator.CreateInstance(tmp.ClassType, _params);

                scope.Stack.Push(instance, Helper.GetDataTypeByObject(instance));

            }

            // Push 
            else if (node is PushFieldNode)
            {
                var ppNode = (node as PushFieldNode);
                var firstStackObj = scope.Stack.Pop(); // Get current object

                if (firstStackObj.DataType == Shared.DataType.AnonymObject)
                {
                    var prop = ((AnonymObject)firstStackObj.Value).GetProperty(ppNode.PropertyName);
                    scope.Stack.Push(prop.Value, prop.DataType);
                }
                else
                {
                    var ty = firstStackObj.Value.GetType();
                    var member = ty.GetProperty(ppNode.PropertyName);
                    object val = member.GetValue(firstStackObj.Value);
                    scope.Stack.Push(val, Helper.GetDataTypeByObject(val));
                }
            }

            else if (node is PopFieldNode)
            {
                var ppNode = (node as PopFieldNode);
                var val = scope.Stack.Pop(); // Get current object
                var firstStackObj = scope.Stack.Pop(); // Get current object

                if (firstStackObj.DataType == Shared.DataType.AnonymObject)
                {
                    ((AnonymObject)firstStackObj.Value).SetProperty(new Variable(ppNode.PropertyName) { Value = val.Value, DataType = val.DataType });
                }
                else
                {
                    var member = firstStackObj.Value.GetType().GetProperty(ppNode.PropertyName);
                    member.SetValue(firstStackObj.Value, val.Value);
                }
            }

            // Push argument to arg stack
            else if (node is PushArgumentNode)
            {
                scope.ArgumentStack.PushFront(new Tuple<int,StackItem>((node as PushArgumentNode).Index, scope.Stack.Pop()));
            }

            // Pop variable
            else if (node is PopVariable)
            {
                StackItem item = scope.Stack.Pop();
                Variable var = scope.GetVariable((node as PopVariable).VariableName);
                var.Value = item.Value;
                var.DataType = item.DataType;
            }

            // Operator
            else if (node is OperatorNode)
            {
                scope.Stack.Execute((node as OperatorNode).OpType);
            }

            else if (node is NewArrayNode)
            {
                var lst = new List<object>();
                scope.Stack.Push(lst, Helper.GetDataTypeByObject(lst));
            }
            else if (node is AddElementNode)
            {
                var toAdd = scope.Stack.Pop();
                var array = scope.Stack.Peek(0);

                (array.Value as List<object>).Add(toAdd.Value);
            }
            else if (node is LoadElementNode)
            {
                var index = scope.Stack.Pop();
                var toIndex = scope.Stack.Pop();

                // Get type for variable to index
                Type tp = toIndex.Value.GetType();

                bool hasIndex = false;

                foreach (var prop in tp.GetProperties())
                {
                    var info = prop.GetIndexParameters();

                    if (info != null && info.Length > 0)
                    {
                        try
                        {
                            var toPush = prop.GetValue(toIndex.Value, new object[] { index.Value });
                            scope.Stack.Push(toPush, Helper.GetDataTypeByObject(toPush));
                            hasIndex = true;
                            break;
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }

                if (!hasIndex)
                {
                    throw new Exception(tp.Name + " has no Index-Property");
                }
            }
            else if (node is PopElementNode)
            {
                var val = scope.Stack.Pop();
                var index = scope.Stack.Pop();
                var toIndex = scope.Stack.Pop();
                bool hasIndex = false;

                // Get type for variable to index
                Type tp = toIndex.Value.GetType();

                foreach (var prop in tp.GetProperties())
                {
                    var info = prop.GetIndexParameters();

                    if (info != null && info.Length > 0)
                    {
                        try
                        {
                            prop.SetValue(toIndex.Value, val.Value, new object[] { index.Value });
                            hasIndex = true;
                            break;
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }

                if (!hasIndex)
                {
                    throw new Exception(tp.Name + " has no Index-Property");
                }
            }
        }
        #endregion

        /// <summary>
        /// Call a SCL+ function
        /// </summary>
        /// <param name="functionName">Name of the function</param>
        /// <param name="args">Argument list</param>
        /// <returns>Return value of the function. Null if no value is returned</returns>
        public object InvokeFunction(string functionName, params object[] args)
        {
            Scope funcScope = new Scope(rootScope);

            if (appStruct.Functions.ContainsKey(functionName) == false)
            {
                throw new Exception("Function not found: " + functionName);
            }

            var funcNode = appStruct.Functions[functionName];

            int i = 0;

            if (args != null)
            {
                foreach (var arg in args)
                {
                    funcScope.ArgumentStack.PushBack(
                        new Tuple<int, StackItem>(i,
                        new StackItem()
                        {
                            DataType = Helper.GetDataTypeByObject(arg),
                            Value = arg
                        }));

                    i++;
                }
            }

            foreach (var child in funcNode.Children)
            {
                ExecuteCommand(child, funcScope);
            }

            return funcScope.Stack.Pop().Value;
        }

        /// <summary>
        /// Set the value of a global variable
        /// </summary>
        /// <param name="name">Name of the variable</param>
        /// <param name="value">New variable value</param>
        public void SetVariable(string name, object value)
        {
            if (rootScope.Vars.ContainsKey(name))
            {
                rootScope.GetVariable(name).Value = value;
                rootScope.GetVariable(name).DataType = Helper.GetDataTypeByObject(value);
            }
            else
            {
                Variable var = new Variable(name);
                var.Value = value;
                var.DataType = Helper.GetDataTypeByObject(value);
                rootScope.Vars.Add(name, var);
            }
        }

        /// <summary>
        /// Get variable value
        /// </summary>
        /// <param name="name">Name of the variable</param>
        /// <returns>Variable value (content)</returns>
        public object GetVariable(string name)
        {
            return rootScope.GetVariable(name).Value;
        }
        #endregion

        #region Public Member

        #endregion
    }
}
