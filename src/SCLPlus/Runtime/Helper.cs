﻿using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    public class Helper
    {
        public static DataType GetDataTypeByObject(object input)
        {
            if (input == null)
            {
                return DataType.Null;
            }
            else if (input is string)
            {
                return DataType.Str;
            }
            else if (input is char)
            {
                return DataType.Char;
            }
            else if (input is bool)
            {
                return DataType.Boolean;
            }
            else if (input is int)
            {
                return DataType.Int32;
            }
            else if (input is long)
            {
                return DataType.Int64;
            }
            else if (input is float)
            {
                return DataType.Float32;
            }
            else if (input is double)
            {
                return DataType.Float64;
            }
            else if (input is AnonymObject)
            {
                return DataType.AnonymObject;
            }
            else
            {
                return DataType.Object;
            }
        }
    }
}
