﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Variable class of the runtime, contains all information about a variable in a scope</summary>

using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    /// <summary>
    /// Variable
    /// </summary>
    internal class Variable
    {
        #region Private Member
        private string name;
        private object value;
        private DataType dataType;
        #endregion

        #region Constructor
        /// <summary>
        /// Crate variable and set name
        /// </summary>
        /// <param name="name">Name of the var</param>
        public Variable(string name)
        {
            this.name = name;
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        
        #endregion

        #region Public Member
        /// <summary>
        /// Get variable name
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Get set variable value
        /// </summary>
        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Variable data type
        /// </summary>
        public DataType DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }
        #endregion
    }
}
