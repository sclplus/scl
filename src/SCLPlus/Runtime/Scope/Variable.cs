﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Argument class of the runtime, contains all information about an argument in a scope</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    /// <summary>
    /// Variable
    /// </summary>
    internal class Argument
    {
        #region Private Member
        private string name;
        private object value;
        #endregion

        #region Constructor
        /// <summary>
        /// Crate variable and set name
        /// </summary>
        /// <param name="name">Name of the var</param>
        public Argument(string name)
        {
            this.name = name;
        }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods
        
        #endregion

        #region Public Member
        /// <summary>
        /// Get variable name
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Get set variable value
        /// </summary>
        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
        #endregion
    }
}
