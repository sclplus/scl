﻿// <copyright file="Token.cs" company="Benedikt Eggers">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Benedikt Eggers</author>
// <date>03/16/2015</date>
// <summary>Parse the IL -Code and create the Command-Chain</summary>

using SCLPlus.Collections.Generic;
using SCLPlus.Runtime.Commands;
using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime
{
    /// <summary>
    /// Parse an IL-Script
    /// </summary>
    public class Parser
    {
        #region Private Member
        private ApplicationStructure appStruct;
        #endregion

        #region Constructor
        /// <summary>
        /// Create the parser
        /// </summary>
        public Parser()
        {
            appStruct = new ApplicationStructure();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Remove a string from the beginning of a token
        /// </summary>
        /// <param name="token">Token</param>
        /// <param name="toRemove">to remove</param>
        /// <returns>Token with the command</returns>
        private string RemoveAtBegining(string token, string toRemove)
        {
            string returnValue = token.Substring(toRemove.Length, token.Length - toRemove.Length);
            returnValue.Trim();

            if (returnValue.StartsWith("."))
            {
                returnValue = returnValue.Substring(1, returnValue.Length - 1);
            }

            return returnValue;
        }

        /// <summary>
        /// Get string until a seperator
        /// </summary>
        /// <param name="token">Complete token</param>
        /// <param name="seperator">Seperator, which will device the string. for example: test() will be test</param>
        /// <returns>String befor the seperator. If no seperator found, the complete token will be returned</returns>
        private string GetUntilSeperator(string token, char seperator)
        {
            string[] split = token.Split(new char[] { seperator });
            if (split.Length == 0)
            {
                return token.Trim();
            }
            else
            {
                return split[0].Trim();
            }
        }

        /// <summary>
        /// Get parenthesis () content
        /// </summary>
        /// <param name="token">Complete token</param>
        /// <returns>List of parenthesis content</returns>
        private IList<string> GetParenthesisContent(string token)
        {
            List<string> returnValue = new List<string>();
            string temp = null;

            for (int i = 0; i < token.Length; i++)
            {
                if (token[i] == '(')
                {
                    temp = "";
                }
                else if (token[i] == ')')
                {
                    if (string.IsNullOrWhiteSpace(temp) == false)
                    {
                        returnValue.Add(temp.Trim());
                    }
                    break;
                }
                else if (token[i] == ',')
                {
                    if (string.IsNullOrWhiteSpace(temp) == false)
                    {
                        returnValue.Add(temp.Trim());
                    }
                    temp = "";
                }
                else if (temp != null)
                {
                    temp += token[i];
                }
            }

            return returnValue;
        }
        #endregion

        #region Public Methods

        #region [Parse]
        internal void Parse(Stream ilCode)
        {
            MemoryStream memStr = (MemoryStream)ilCode;
            string code = Encoding.UTF8.GetString(memStr.ToArray());

            // Create queue with all lines as a token
            Dequeue<string> tokens = new Dequeue<string>(code.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));

            // Create root node of every application
            appStruct.Root = new RootNode(null);

            while (tokens.Count > 0)
            {
                string token = tokens.PopFirst();

                Parse(appStruct.Root, token, tokens);
            }
        }
        #endregion

        /// <summary>
        /// Parse a single chainnode token
        /// </summary>
        /// <param name="node">Parent chain node instance</param>
        /// <param name="token">Token as string</param>
        /// <param name="tokens">Token qeue</param>
        private void Parse(CommandChainNode node, string token, Dequeue<string> tokens)
        {
            // Trim token
            token = token.Trim();

            #region [Comment]
            if (token.StartsWith("//"))
            {
                // Some IL-Code command, no need to take care of yet
            }
            #endregion

            #region [.module]
            // Define module name
            else if (token.StartsWith(".module"))
            {

            }
            #endregion

            #region [.function]
            // Function declaration
            else if (token.StartsWith(".function"))
            {
                // create function node
                var funcNode = new FunctionNode(node);

                //
                string cmdLess = RemoveAtBegining(token, ".function");
                funcNode.FunctionName = GetUntilSeperator(cmdLess, '(');
                funcNode.ArgumentDecl = GetParenthesisContent(token);

                // add to global function list
                if (appStruct.Functions.ContainsKey(funcNode.FunctionName))
                {
                    throw new Exception("Function already exists: " + funcNode.FunctionName);
                }

                appStruct.Functions.Add(funcNode.FunctionName, funcNode);
                
                // Get the content of the function
                var tempTokens = GetInnerTokens(tokens, "{", "}");

                // Remove { }
                tempTokens.PopFirst();
                tempTokens.PopLast();

                while(tempTokens.Count > 0)
                {
                    string tempToken = tempTokens.PopFirst();

                    // parse the content of the function and attach all nodes to the Function-Header
                    Parse(funcNode, tempToken, tempTokens);
                }
            }
            #endregion

            #region [.anonym.class]
            else if (token.StartsWith(".anonym.class"))
            {
                // create function node
                var aoNode = node.CreateNode<AnonymObjectNode>();
                
                var tempTokens = GetInnerTokens(tokens, "{", "}");

                tempTokens.PopFirst();
                tempTokens.PopLast();

                while (tempTokens.Count > 0)
                {
                    string tempToken = tempTokens.PopFirst();

                    Parse(aoNode, tempToken, tempTokens);
                }
            }
            #endregion

            #region [ldarg]
            else if (token.StartsWith("ldarg"))
            {
                string cmdLess = RemoveAtBegining(token, "ldarg.");
                var argNode = node.CreateNode<PushArgumentNode>();
                argNode.Index = int.Parse(cmdLess);
            }
            #endregion

            #region [ret]
            // Return from function
            else if (token.StartsWith("ret"))
            {
                node.CreateNode<ReturnNode>();
            }
            #endregion

            #region [.locals]
            // define all locals
            else if (token.StartsWith(".locals"))
            {
                string cmdLess = RemoveAtBegining(token, ".locals");

                var lNode = node.CreateNode<LocalsNode>();
                lNode.VariableDecl.AddRange(GetParenthesisContent(cmdLess));
            }
            #endregion

            #region [.maxstack]
            // Define maximum stack size
            else if (token.StartsWith(".maxstack"))
            {
                string cmdLess = RemoveAtBegining(token, ".maxstack");
                IList<string> size = GetParenthesisContent(cmdLess);
            }
            #endregion

            #region [call.f]
            // Call a function
            else if (token.StartsWith("call.f"))
            {
                string cmdLess = RemoveAtBegining(token, "call.f");
                IList<string> args = GetParenthesisContent(token);

                var cfNode = node.CreateNode<CallFunctionNode>();
                cfNode.ParameterCount = args.Count;
                cfNode.FunctionName = GetUntilSeperator(cmdLess, '(');
            }
            #endregion

            #region [call.m]
            else if (token.StartsWith("call.m"))
            {
                string cmdLess = RemoveAtBegining(token, "call.f");
                IList<string> args = GetParenthesisContent(token);

                var cfNode = node.CreateNode<CallMethodNode>();
                cfNode.ParameterCount = args.Count;
                cfNode.MethodName = GetUntilSeperator(cmdLess, '(');
            }
            #endregion

            #region [pop]
            else if (token.Trim() == "pop")
            {
                var popNode = node.CreateNode<PopNode>();
            }
            #endregion

            #region [ldc]
            // Load constant to the stack
            else if (token.StartsWith("ldc"))
            {
                string cmdLess = RemoveAtBegining(token, "ldc");
                string type = GetUntilSeperator(cmdLess, ' ').Trim();
                cmdLess = RemoveAtBegining(cmdLess, type).Trim();

                var pcNode = node.CreateNode<PushConstantNode>();

                string val = cmdLess.Trim();

                if (val.StartsWith("\"") && val.EndsWith("\""))
                {
                    val = val.Substring(1, val.Length - 2);
                }

                pcNode.DataType = DataTypeHelper.StrToDataType(type);
                pcNode.ConstantValue = DataTypeHelper.StringValueToObject(val, pcNode.DataType);
            }
            #endregion

            #region [newobj]
            else if (token.StartsWith("newobj"))
            {
                string cmdLess = RemoveAtBegining(token, "newobj").Trim();
                string type = GetUntilSeperator(cmdLess, ' ').Trim();
                cmdLess = GetUntilSeperator(RemoveAtBegining(cmdLess, type), '(');
                
                IList<string> args = GetParenthesisContent(token);

                if (type == "instance")
                {
                    IList<string> path = null;

                    string[] splitted = cmdLess.Split(new string[] { ".", "::" }, StringSplitOptions.RemoveEmptyEntries);

                    path = splitted.ToArray<string>();

                    var extClass = node.CreateNode<CreateExternClassInstanceNode>();
                    extClass.ParameterCount = args.Count;
                    extClass.Path = path;
                    extClass.ClassName = path.Last();

                    extClass.ClassType = GetType(cmdLess.Replace("::", "."));
                }
            }
            #endregion

            #region [ldnull]
            else if (token.StartsWith("ldnull"))
            {
                var pcNode = node.CreateNode<PushConstantNode>();
                pcNode.ConstantValue = null;
            }
            #endregion

            #region [ldloc]
            else if (token.StartsWith("ldloc"))
            {
                string cmdLess = RemoveAtBegining(token, "ldloc");

                var ppNode = node.CreateNode<PushVariableNode>();
                ppNode.VariableName = cmdLess;
            }
            #endregion

            #region [oldclass]
            else if (token.StartsWith("oldclass"))
            {
                string cmdLess = RemoveAtBegining(token, "oldclass");

                string type = GetUntilSeperator(cmdLess, ' ');
                cmdLess = RemoveAtBegining(cmdLess, type);

                IList<string> path = null;

                string[] splitted = cmdLess.Split(new string[] { ".", "::" }, StringSplitOptions.RemoveEmptyEntries);

                path = splitted.ToArray<string>();

                if (type == "ext")
                {
                    var extClass = node.CreateNode<LoadExternClassNode>();
                    extClass.Path = path;
                    extClass.ClassName = path.Last();

                    extClass.ClassType = GetType(cmdLess.Replace("::", "."));
                }
                else
                {

                }
            }
            #endregion

            #region [ldfld]
            else if (token.StartsWith("ldfld"))
            {
                string cmdLess = RemoveAtBegining(token, "ldfld");

                var ppNode = node.CreateNode<PushFieldNode>();
                ppNode.PropertyName = cmdLess;
            }
            #endregion

            #region [stfld]
            else if (token.StartsWith("stfld"))
            {
                string cmdLess = RemoveAtBegining(token, "stfld");

                var ppNode = node.CreateNode<PopFieldNode>();
                ppNode.PropertyName = cmdLess;
            }
            #endregion
                
            #region [stloc]
            // Pop value from stack to variable
            else if (token.StartsWith("stloc"))
            {
                string cmdLess = RemoveAtBegining(token, "stloc");

                var plNode = node.CreateNode<PopVariable>();
                plNode.VariableName = cmdLess.Trim();
            }
            #endregion
                
            #region [add]
            else if (token == ("add"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Add;
            }
            #endregion

            #region [sub]
            else if (token == ("sub"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Sub;
            }
            #endregion

            #region [mul]
            else if (token == ("mul"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Mul;
            }
            #endregion

            #region [div]
            else if (token == ("div"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Div;
            }
            #endregion

            #region [eq]
            else if (token == ("eq"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Equal;
            }
            #endregion

            #region [ueq]
            else if (token == ("ueq"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Unequal;
            }
            #endregion

            #region [gt]
            else if (token == ("gt"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Greater;
            }
            #endregion

            #region [sm]
            else if (token == ("sm"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Smaller;
            }
            #endregion

            #region [gteq]
            else if (token == ("gteq"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.GreaterEqual;
            }
            #endregion

            #region [smeq]
            else if (token == ("smeq"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.SmallerEqual;
            }
            #endregion

            #region [and]
            else if (token == ("and"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.And;
            }
            #endregion

            #region [or]
            else if (token == ("or"))
            {
                var opNode = node.CreateNode<OperatorNode>();
                opNode.OpType = Commands.OperatorType.Or;
            }
            #endregion

            else if (token == "newarr")
            {
                node.CreateNode<NewArrayNode>();
            }

            else if (token == "addelem")
            {
                node.CreateNode<AddElementNode>();
            }

            else if (token == "ldelem")
            {
                node.CreateNode<LoadElementNode>();
            }

            else if (token == "stelem")
            {
                node.CreateNode<PopElementNode>();
            }

            #region [Token not found]
            else
            {
                throw new Exception("Token not expected in the runtime: " + token);
            }
            #endregion
        }

        #region [GetType]
        private Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
        #endregion

        #region [GetInnerTokens]
        private Dequeue<string> GetInnerTokens(Dequeue<string> tokens, string openToken, string closeToken)
        {
            Dequeue<string> returnList = new Dequeue<string>();

            int bracketCounting = 0;
            while (tokens.Count > 0)
            {
                string token = tokens.PopFirst().Trim();

                if (token == openToken)
                {
                    bracketCounting++;
                }
                else if (token == closeToken)
                {
                    bracketCounting--;
                }

                returnList.PushBack(token);

                // Remove added token
                if (bracketCounting == 0)
                {
                    break;
                }
            }

            return returnList;
        }
        #endregion

        #endregion

        #region Public Member
        /// <summary>
        /// Get the created / parsed app structure
        /// </summary>
        internal ApplicationStructure AppStruct
        {
            get { return appStruct; }
        }
        #endregion
    }
}
