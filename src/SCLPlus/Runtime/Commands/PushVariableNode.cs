﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class PushVariableNode : CommandChainNode
    {
        public PushVariableNode(CommandChainNode parent)
            : base(parent)
        {

        }

        public string VariableName
        {
            get;
            set;
        }
    }
}
