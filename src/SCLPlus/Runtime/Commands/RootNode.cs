﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class RootNode : CommandChainNode
    {
        public RootNode(CommandChainNode parent)
            : base(parent)
        {

        }

    }
}
