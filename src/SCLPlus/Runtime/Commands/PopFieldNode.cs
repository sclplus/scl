﻿using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class PopFieldNode : CommandChainNode
    {
        public PopFieldNode(CommandChainNode parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Name of the var
        /// </summary>
        public string PropertyName
        {
            get;
            set;
        }
        

        public DataType DataType
        {
            get;
            set;
        }
    }
}
