﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class LoadExternClassNode : CommandChainNode
    {
        public LoadExternClassNode(CommandChainNode parent)
            : base(parent)
        {

        }

        public IList<string> Path
        {
            get;
            set;
        }

        public string ClassName
        {
            get;
            set;
        }

        public Type ClassType
        {
            get;
            set;
        }
    }
}
