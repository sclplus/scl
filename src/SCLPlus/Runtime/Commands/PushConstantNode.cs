﻿using SCLPlus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class PushConstantNode : CommandChainNode
    {
        public PushConstantNode(CommandChainNode parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Value of the current constant
        /// </summary>
        public object ConstantValue
        {
            get;
            set;
        }

        public DataType DataType
        {
            get;
            set;
        }
    }
}
