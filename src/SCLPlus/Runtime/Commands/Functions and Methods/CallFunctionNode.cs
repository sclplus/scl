﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class CallFunctionNode : CommandChainNode
    {
        public CallFunctionNode(CommandChainNode parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Name of the current function
        /// </summary>
        public string FunctionName
        {
            get;
            set;
        }

        public List<string> VariableDecl
        {
            get;
            private set;
        }

        public int ParameterCount
        {
            get;
            set;
        }
    }
}
