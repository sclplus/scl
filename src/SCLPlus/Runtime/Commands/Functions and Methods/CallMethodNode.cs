﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class CallMethodNode : CommandChainNode
    {
        public CallMethodNode(CommandChainNode parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Name of the current function
        /// </summary>
        public string MethodName
        {
            get;
            set;
        }

        public int ParameterCount
        {
            get;
            set;
        }
    }
}
