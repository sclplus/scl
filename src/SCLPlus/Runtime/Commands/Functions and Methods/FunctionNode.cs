﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class FunctionNode : CommandChainNode
    {
        public FunctionNode(CommandChainNode parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Name of the current function
        /// </summary>
        public string FunctionName
        {
            get;
            set;
        }

        public IList<string> ArgumentDecl
        {
            get;
            set;
        }
    }
}
