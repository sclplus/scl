﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class PushArgumentNode : CommandChainNode
    {
        public PushArgumentNode(CommandChainNode parent)
            : base(parent)
        {

        }

        public int Index
        {
            get;
            set;
        }
    }
}
