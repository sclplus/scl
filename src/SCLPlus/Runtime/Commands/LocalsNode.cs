﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class LocalsNode : CommandChainNode
    {
        public LocalsNode(CommandChainNode parent)
            : base(parent)
        {
            VariableDecl = new List<string>();
        }

        public List<string> VariableDecl
        {
            get;
            private set;
        }
    }
}
