﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class PopElementNode : CommandChainNode
    {
        public PopElementNode(CommandChainNode parent)
            : base(parent)
        {

        }
    }
}
