﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Runtime.Commands
{
    internal class PopVariable : CommandChainNode
    {
        public PopVariable(CommandChainNode parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Name of the var
        /// </summary>
        public string VariableName
        {
            get;
            set;
        }
    }
}
