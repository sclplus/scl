﻿using SCLPlus.Compiler;
using SCLPlus.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Lib.StdLib
{
    public class StandardLibraryHelper
    {
        public static void Init(SCLRuntime runtime)
        {
            // Execute default libs
            string lib = LoadEmbeddedRessource("SCLPlus.Lib.Library.scl");

            Compiler.SCLCompiler compiler = new Compiler.SCLCompiler(new StdErrorListener());
            compiler.AddCodeFragment(lib, "DefaultLib", CodeFragmentType.None);
            var compiledSciprt = compiler.Compile(false);

            // Execute in runtime
            runtime.Execute(compiledSciprt.Stream);
        }

        private static string LoadEmbeddedRessource(string name)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            string res = "";
            using (Stream stream = assembly.GetManifestResourceStream(name))
            using (StreamReader reader = new StreamReader(stream))
            {
                res = reader.ReadToEnd();
            }

            return res;
        }
    }

    internal class StdErrorListener : IErrorListener
    {

        public void Report(string errorCode, string errorMessage, int index, int length, RawToken token)
        {

        }
    }
}
