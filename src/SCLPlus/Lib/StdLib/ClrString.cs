﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLPlus.Shared.StdLib
{
    public class ClrString
    {
        public static string ToString(object toString)
        {
            if (toString == null)
            {
                throw new ArgumentNullException("toString");
            }
            
            return toString.ToString();
        }
    }
}
